# Gazelle Migration

__Migration__ is a module that run installation or upgrade scripts at application deployment. It
focuses on schema and data migration and is based
on [FlywayDB](https://flywaydb.org/documentation/) (version 4.2.0 for JDK 7 compatibility).

The module has its own table in the database (named `version_schema`) to keep track of already ran
migrations and is able to know if new migration should be applied or not.

__Migration__ module is designed to be integrated with a JPA container, Hibernate and PostgreSQL
database server. H2 database is also supported but only for tests.

## Usage

### Dependencies

__Migration__ requires Java 7 or higher, JPA 2.0 and Hibernate 4.3.x.

Add __Migration__ artifact as dependency to your project

```xml

<dependency>
    <groupId>net.ihe.gazelle.maven</groupId>
    <artifactId>gazelle-migration</artifactId>
    <version>${version.gazelle-tools}</version>
</dependency>
```

Any other database management tools, such as __Hibernate hbm2ddl__, should be disabled or used only
for validation. See [Verify JPA Entity schema](#verify-jpa-entity-schema).

### Script migrations

__Migration__ can run provided SQL or Java migration scripts at application deployment to upgrade
the schema and data.

By default, all __SQL or Java migration__ scripts should be located in the classpath
in `/db/migration` (SQL in `src/main/resources/db/migration` for maven projects and Java in
package`db.migration`). _Default migration classpath can be overridden in the configuration (
See [Migration configuration](#migration-configuration))._

#### Single version migration

Single version migrations are scripts ordered by version value, and they will be run only once by
__Migration__ module as soon they are on the classpath. If a newly provided migration script is not
greater in order than already applied migrations, __Migration__ module will fail to apply it. As
example, if V1.0.0 and V1.2.0 migrations are already applied but a new V1.0.3 migration is provided.

* Single version migration script names must be formated as `V1_0_0__script-name.sql`. This is a
  __FlywayDB convention__:
    * `V` : single version migration.
    * `1_`: digit for version identification separated with underscores `_` (must match application
      version targeted).
    * `__`: Two consecutive underscores to seperate version-number from script name.
    * __script name__: human-readable name, may contain Jira ticket number.
    * Unfortunatly, qualifiers - like alpha, beta, RC... - cannot be used in Flyway's version
      numbers. However, any sub-version numbers can be added. `6.0.0-RC1` could be identified
      as `V6_0_0_1`.

Placeholders can be used in single version migrations. See [Placeholders](#placeholders).

The module only helps running the scripts, writting them is out of scope.

#### Repeatable migration

__Migration__ module also supports repeatable migrations. Those migrations are re-run every time
their content change. It is your responsability to use statements that can be run several times
as `...IF EXISTS...` or `UPDATE...`.

* Repeatable migration script names must be formated as `R__script-name.sql`. This is a
  __FlywayDB convention__:
    * `R` : repeatable migration.
    * `__`: Two consecutive underscores to seperate type from script-name.
    * __script-name__: human-readable name.

Placeholders can be used in repeatable migrations (see [Placeholders](#placeholders)). But changing
the value of one or more placeholders will not re-trigger the run (only available on later version
of FlywayDB.

#### Java migration

__Migration__ module also supports migrations in plain Java language. Java migration must implement
the Flyway interface `org.flywaydb.core.api.migration.jdbc.JdbcMigration` and must respect the
naming format of [single version](#single-version-migration) or [repeatable](#repeatable-migration)
migration, but with `.java` file extension.

```java
package net.ihe.gazelle.java.migration;

import org.flywaydb.core.api.migration.jdbc.JdbcMigration;

import java.sql.Connection;

public class V1_0_0__JavaExampleMigration implements JdbcMigration {

   @Override
   public void migrate(Connection connection) throws Exception {
      //... 
   }
}
```

#### Callback scripts

FlywayDB is defining a large number of callback endpoint, that are able to run provided scripts. See
[FlywayDB Callbacks](https://flywaydb.org/documentation/concepts/callbacks).

For industrialization, We recommand to use the `afterMigrate` callback to update
application-preferences according to the placeholders.

1. Create at least one script named `afterMigrate.sql` in one of the defined migration classpath.
2. Populate the script with update statement of mandatory deployment preferences
   ```sql
   UPDATE application_preference
     SET value = '${testbed.url}/example'
     WHERE name = 'application_url';
   UPDATE application_preference
     SET value = '${sso.enabled}'
     WHERE name = 'cas_enabled';
   -- etc...
   ```
3. Provide a placeholders properties file as described in [Placeholders](#placeholders)
   and [Configuration](#migration-configuration).
4. __Migration__ module will run it everytime all migration are applied or verified and successful
   (at each application restart).

### Placeholders

Placeholders are variables that are replaced by their values at script execution. Placeholders are
expressed with a dollar sign and brackets including a property name: `${GZL_URL}`
or `${application.url}`.

By default, all environment variables accessible by the JVM will be given to the __Migration__
Module to be interpreted as placeholder value.

If one or more placeholders are missing, __Migration__ will fail.

#### Placeholders as properties file

A key-value map can also be provided as properties file :

```properties
application.url=http://localhost/app
application.oid=1.2.3.4
```

If used via properties file, the path must be defined in the [configuration](#migration-configuration).

Placeholders lookup priority is :
1. Environment variables
2. Properties file

Which means that environment variables will override properties file values.

_Note that environment variables are preferred over properties-file for DevOps practices._

### Migration configuration

The __Migration__ module __requires__ configuration to get data-source and scripts path. This
configuration must be provided as properties in the `META-INF/persistence.xml`.

| property name                                | description                                                                   | default value |
|----------------------------------------------|-------------------------------------------------------------------------------|---------------|
| net.ihe.gazelle.migration.enabled            | Enable/disable migration module                                               | false         |
| net.ihe.gazelle.migration.classpaths         | SQL and Java migration paths, several can be given and separated by comma ',' | /db/migration |
| net.ihe.gazelle.migration.baseline.classpath | Baseline script path                                                          | /db/baseline  |
| net.ihe.gazelle.migration.placeholders.path  | Placeholder properties file path                                              | null          |

Here is an example of `persistence.xml` configuration:

```xml

<persistence xmlns="http://java.sun.com/xml/ns/persistence"
             xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
             xsi:schemaLocation="http://java.sun.com/xml/ns/persistence http://java.sun.com/xml/ns/persistence/persistence_2_0.xsd"
             version="2.0">
    <persistence-unit name="ExamplePersistenceUnit">

        <provider>org.hibernate.ejb.HibernatePersistence</provider>
        <jta-data-source>java:jboss/datasources/ExampleDS</jta-data-source>

        <jar-file>...</jar-file>
        <class>...</class>

        <properties>

            <!-- migration properties -->
            <property name="net.ihe.gazelle.migration.enabled" value="true"/>
            <property name="net.ihe.gazelle.migration.classpaths"
                      value="/example/migration1,/example/migration2"/>
            <property name="net.ihe.gazelle.migration.baseline.classpath"
                      value="/example/baseline"/>
            <property name="net.ihe.gazelle.migration.placeholders.path"
                      value="/etc/example/placeholders.properties"/>

            <!-- other -->
            <property name="..." value="..."/>

        </properties>
    </persistence-unit>
</persistence>
```

__Migration__ is also able to operate with JPA JDBC properties. Define the following in
the `persistence.xml` instead of a `jta-data-source` or `non-jta-data-source`:

```xml

<properties>
    <!-- Configuring JDBC properties -->
    <property name="javax.persistence.jdbc.url" value="jdbc:postgresql:example-db"/>
    <property name="javax.persistence.jdbc.driver" value="org.postgresql.Driver"/>
    <property name="javax.persistence.jdbc.user" value="user"/>
    <property name="javax.persistence.jdbc.password" value="password"/>

    <!-- other... -->
</properties>
```

### Migration at startup

__Migration__ comes with an __Hibernate__ integrator that automatically starts, verify and run
migrations at application deployment when the JPA Container starts. Any failure in a migration will
stop the deployment.

Any call to `Persistence.createEntityManagerFactory("persistenceUnitName");` will also trigger
migration module.

### Using Migration with an application that already had an unmanaged database.

When deploying an application with an existing database and with __Migration__ embedded for the
first time, the module will tag the existing schema and data as the __baseline__
(version `0.0.0`), and will be able to apply all migration scripts onto this baseline.

The baseline schema `schema.sql` and init `init.sql` scripts must BOTH be provided by default in the
classpath `db/baseline` (in `src/main/resources/db/baseline` for maven projects). __Those baseline
scrips will be run only in the case of a new installation__ (aka, database is created but empty).
Once the schema and init are run, the module will tag the baseline and run other migration scripts
as usual.

```
+- db/baseline
   +- schema.sql
   +- init.sql
```

Default baseline classpath can be overridden in the configuration (
See [Migration configuration](#migration-configuration)).

_For new projects that does not have an existing database, baseline is not required, schema and init
should entirely be built as migration._

### Verify JPA Entity schema

It is possible to also requires Hibernate to verify that JPA Entities have all required relations
(tables, sequences, constraints...). To do so, set the value of the property
`hibernate.hbm2ddl.auto` to `validate` in `persistence.xml`.

```xml

<property name="hibernate.hbm2ddl.auto" value="validate"/>
```

_Migration module is using an Hibernate Integrator that garanties that the schema validation is
performed after the migration_.

### Migration module and unit tests

Some unit tests may be using specific persistence-units for tests with schema generation or data
imports. If migration is messing your tests, it can be disabled in your `persistence.xml`:

```xml

<property name="net.ihe.gazelle.migration.enabled" value="false"/>
```

__Migration__ also works with __H2 in-memory__ database, however SQL syntax support is lower on h2
than PostgreSQL, and some of your scripts may fail on h2. Some example:

* `pg_dump` write constraint creation such as
  `ALTER TABLE ONLY public.my_table ADD CONSTRAINT my_table_pkey PRIMARY KEY (id);`, but `ONLY`
  is not supported by h2.
* H2 does not support `ALTER SEQUENCE ... RENAME TO ...`, but
  only `ALTER SEQUENCE ... RESTART WITH ...`.
* etc

### Logging

Migration module is using sl4j api logging. A logging implementation should be provided at runtime (
provided by default if deploying on JBoss / Wildfly).
See [binding](https://www.slf4j.org/manual.html#swapping).

If you need to configure specific handlers, appenders and levels, the loggers are

* `net.ihe.gazelle.migration.*`: logging migration process information.
* `org.flywaydb.*`: logging detailed info about datasource and migration scripts verified/applied.
* `org.hibernate.tool.hbm2ddl.*`: logging JPA Entity mapping validation (if enabled).
