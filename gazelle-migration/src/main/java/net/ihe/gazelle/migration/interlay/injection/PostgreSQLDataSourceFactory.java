package net.ihe.gazelle.migration.interlay.injection;

import org.postgresql.ds.PGSimpleDataSource;

import javax.sql.DataSource;

class PostgreSQLDataSourceFactory extends AbstractDataSourceFactory {

   @Override
   DataSource getDataSource(String url, String user, String password) {
      PGSimpleDataSource dataSource = new PGSimpleDataSource();
      dataSource.setURL(url);
      dataSource.setUser(user);
      dataSource.setPassword(password);
      return dataSource;
   }

}
