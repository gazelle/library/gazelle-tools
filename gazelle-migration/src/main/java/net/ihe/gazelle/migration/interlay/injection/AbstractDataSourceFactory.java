package net.ihe.gazelle.migration.interlay.injection;

import net.ihe.gazelle.migration.application.MigrationException;

import javax.sql.DataSource;

public abstract class AbstractDataSourceFactory {

   private static final String POSTGRESQL_DRIVER = "org.postgresql.Driver";
   private static final String H2_DRIVER = "org.h2.Driver";

   static AbstractDataSourceFactory resolveFactory(String driver) {
      if (driver.equals(POSTGRESQL_DRIVER)) {
         return new PostgreSQLDataSourceFactory();
      } else if (driver.equals(H2_DRIVER)) {
         return new H2DataSourceFactory();
      } else {
         throw new MigrationException(
               String.format("Invalid driver '%s', Migration module only support " + POSTGRESQL_DRIVER, driver)
         );
      }
   }

   abstract DataSource getDataSource(String url, String user, String password);

}
