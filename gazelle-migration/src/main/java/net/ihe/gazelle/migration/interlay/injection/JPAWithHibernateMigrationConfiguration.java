package net.ihe.gazelle.migration.interlay.injection;

import net.ihe.gazelle.migration.application.MigrationException;
import org.hibernate.cfg.AvailableSettings;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 * MigrationConfiguration extracted from JPA persistence.xml AND through Hibernate provider :(
 */
public class JPAWithHibernateMigrationConfiguration extends AbstractEnvMigrationConfiguration {

   public static final String MIGRATION_ENABLED = "net.ihe.gazelle.migration.enabled";
   public static final String MIGRATION_CLASSPATHS = "net.ihe.gazelle.migration.classpaths";
   public static final String BASELINE_CLASSPATH = "net.ihe.gazelle.migration.baseline.classpath";
   public static final String PLACEHOLDERS_PATH = "net.ihe.gazelle.migration.placeholders.path";
   public static final String JDBC_URL = "javax.persistence.jdbc.url";
   public static final String JDBC_DRIVER = "javax.persistence.jdbc.driver";
   public static final String JDBC_USER = "javax.persistence.jdbc.user";
   public static final String JDBC_PASSWORD = "javax.persistence.jdbc.password";


   private final Properties jpaProperties;

   public JPAWithHibernateMigrationConfiguration(Properties jpaProperties) {
      this.jpaProperties = jpaProperties;
   }

   @Override
   public boolean isMigrationEnabled() {
      return Boolean.parseBoolean(jpaProperties.getProperty(MIGRATION_ENABLED));
   }

   @Override
   public DataSource getDataSource() {
      DataSource dataSource = getDataSourceFromHibernate();
      if (dataSource != null) {
         return dataSource;
      } else {
         dataSource = getDataSourceFromJDBCProperties();
         if (dataSource != null) {
            return dataSource;
         } else {
            throw new MigrationException("Unable to create data-source from JPA persistence.xml. Neither " +
                  "DataSource nor javax.persistence.jdbc configuration provided.");
         }
      }
   }

   @Override
   public String[] getMigrationClassPaths() {
      String path = jpaProperties.getProperty(MIGRATION_CLASSPATHS);
      return path != null ? path.split(",") : null;
   }

   @Override
   public String getBaselineClassPath() {
      return jpaProperties.getProperty(BASELINE_CLASSPATH);
   }

   @Override
   public Map<String, String> getPlaceholders() {
      Map<String, String> placeholders = new HashMap<>();
      String placeholdersPath = jpaProperties.getProperty(PLACEHOLDERS_PATH);
      if(placeholdersPath != null) {
         placeholders.putAll(new PlaceholdersPropertiesFactory(placeholdersPath).getPlaceholders());
      }
      placeholders.putAll(super.getPlaceholders());
      return placeholders;
   }

   private boolean isDefined(String value) {
      return value != null && !value.isEmpty();
   }

   private DataSource getDataSourceFromHibernate() {
      return (DataSource) jpaProperties.get(AvailableSettings.DATASOURCE);
   }

   private DataSource getDataSourceFromJDBCProperties() {
      String url = jpaProperties.getProperty(JDBC_URL);
      String driver = jpaProperties.getProperty(JDBC_DRIVER);
      String user = jpaProperties.getProperty(JDBC_USER);
      String password = jpaProperties.getProperty(JDBC_PASSWORD);
      if (isDefined(url) && isDefined(driver) && isDefined(user) && isDefined(password)) {
         return AbstractDataSourceFactory.resolveFactory(driver).getDataSource(url, user, password);
      }
      return null;
   }

}
