package net.ihe.gazelle.migration.application;

public class MigrationException extends RuntimeException {
   private static final long serialVersionUID = -3070157298622713057L;

   public MigrationException(String message) {
      super(message);
   }

   public MigrationException(String message, Throwable throwable) {
      super(message, throwable);
   }

}
