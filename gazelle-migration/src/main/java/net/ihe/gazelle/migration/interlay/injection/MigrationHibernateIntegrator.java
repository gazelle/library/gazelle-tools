package net.ihe.gazelle.migration.interlay.injection;

import net.ihe.gazelle.migration.application.MigrationService;
import org.hibernate.cfg.Configuration;
import org.hibernate.engine.spi.SessionFactoryImplementor;
import org.hibernate.integrator.spi.Integrator;
import org.hibernate.metamodel.source.MetadataImplementor;
import org.hibernate.service.spi.SessionFactoryServiceRegistry;

public class MigrationHibernateIntegrator implements Integrator {

   @Override
   public void integrate(Configuration configuration, SessionFactoryImplementor sessionFactory,
                         SessionFactoryServiceRegistry serviceRegistry) {
      MigrationService migrationService = new MigrationService(
            new JPAWithHibernateMigrationConfiguration(configuration.getProperties())
      );
      migrationService.run();
   }

   @Override
   public void integrate(MetadataImplementor metadata, SessionFactoryImplementor sessionFactory,
                         SessionFactoryServiceRegistry serviceRegistry) {
      // Unused.
   }

   @Override
   public void disintegrate(SessionFactoryImplementor sessionFactory, SessionFactoryServiceRegistry serviceRegistry) {
      // Unused.
   }
}
