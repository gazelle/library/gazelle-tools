package net.ihe.gazelle.migration.interlay.adapter;

import org.flywaydb.core.internal.dbsupport.DbSupport;
import org.flywaydb.core.internal.dbsupport.h2.H2DbSupport;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

class H2Specifics implements DbSpecifics {

   @Override
   public boolean isSchemaCreated(Connection connection) throws SQLException {
      ResultSet resultSet = connection.getMetaData().getTables(null, null, null, new String[]{"TABLE", "VIEW"});
      return resultSet.getFetchSize() > 0;
   }

   @Override
   public DbSupport getDbSupport(Connection connection) {
      return new H2DbSupport(connection);
   }
}
