CREATE TABLE public.map (
                                                   key character varying(64) NOT NULL,
                                                   value character varying(512)
);

ALTER TABLE public.map
    ADD CONSTRAINT map_pkey PRIMARY KEY (key);