INSERT INTO test_application_preference (name, class, value, description)
    VALUES ('application_url', 'java.lang.String', '${application_url}', 'Fully qualified domain name of the application');
INSERT INTO test_application_preference (name, class, value, description)
    VALUES ('sso_enabled', 'java.lang.Boolean', '${sso_enabled}', 'Enable/disable Single Sign-On');
