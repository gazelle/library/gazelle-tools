--
-- Table public.test_application_preference
--

CREATE TABLE public.test_application_preference
(
    name        character varying(127) NOT NULL,
    class       character varying(255) NOT NULL,
    value       character varying(255),
    description character varying(511)
);

ALTER TABLE ONLY public.test_application_preference
    ADD CONSTRAINT test_application_preference_pkey PRIMARY KEY (name);

--
-- Table public.test_test_run
--

CREATE TABLE public.test_test_run
(
    id integer NOT NULL,
    test_id character varying(127) NOT NULL,
    date timestamp without time zone,
    result character varying(31)
);

ALTER TABLE ONLY public.test_test_run
    ADD CONSTRAINT test_test_run_pkey PRIMARY KEY (id);

CREATE SEQUENCE public.test_test_run_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

--
-- Table public.test_test_step_run
--

CREATE TABLE public.test_test_step_run
(
    id integer NOT NULL,
    step_id character varying(127) NOT NULL,
    test_run_id integer NOT NULL,
    result character varying(31)
);

ALTER TABLE ONLY public.test_test_step_run
    ADD CONSTRAINT test_test_step_run_pkey PRIMARY KEY (id);

ALTER TABLE ONLY public.test_test_step_run
    ADD CONSTRAINT fk_test_step_run_test_run_id FOREIGN KEY (test_run_id) REFERENCES public.test_test_run(id);

CREATE SEQUENCE public.test_test_step_run_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;