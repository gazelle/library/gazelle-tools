INSERT INTO test_application_preference (name, class, value, description)
VALUES ('fqdn', 'java.lang.String', 'localhost', 'Fully qualified domain name of the application');
INSERT INTO test_application_preference (name, class, value, description)
VALUES ('sso_enabled', 'java.lang.Boolean', 'true', 'Enable/disable Single Sign-On');
INSERT INTO test_application_preference (name, class, value, description)
VALUES ('application_url', 'java.lang.String', 'http://localhost', 'Application URL');
