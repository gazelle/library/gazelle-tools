INSERT INTO test_application_preference (name, class_name, value, description)
VALUES ('fqdn', 'java.lang.String', 'localhost', 'Fully qualified domain name of the application');
INSERT INTO test_application_preference (name, class_name, value, description)
VALUES ('sso_enabled', 'java.lang.Boolean', 'true', 'Enable/disable Single Sign-On');
