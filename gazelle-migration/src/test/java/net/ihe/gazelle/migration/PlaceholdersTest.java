package net.ihe.gazelle.migration;

import net.ihe.gazelle.migration.application.MigrationService;
import org.flywaydb.core.api.FlywayException;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

public class PlaceholdersTest {

   private DataSource dataSource;

   @Before
   public void setUp() throws SQLException {
      JdbcTestServer.resetDatabase(JdbcTestServer.TEST_DATABASE);
      dataSource = JdbcTestServer.buildLocalDatasource(JdbcTestServer.TEST_DATABASE);
   }

   @After
   public void tearDown() throws SQLException {
      dataSource.getConnection().close();
   }

   @Test
   public void testMigrationWithPlaceholders() throws SQLException {
      Map<String, String> placeholders = new HashMap<>();
      placeholders.put("application_url", "http://localhost/test");
      placeholders.put("sso_enabled", "false");

      MigrationService migrationService = setUpMigrationService(placeholders, "/db/placeholders/migration");
      migrationService.run();

      Assert.assertEquals("http://localhost/test", getPreferenceValue("application_url", dataSource));
      Assert.assertEquals("false", getPreferenceValue("sso_enabled", dataSource));
   }

   @Test(expected = FlywayException.class)
   public void testMissingAllPlaceholdersError() {
      MigrationService migrationService = setUpMigrationService(null, "/db/placeholders/migration");
      migrationService.run();
   }

   @Test(expected = FlywayException.class)
   public void testMissingOnePlaceholderError() {
      Map<String, String> placeholders = new HashMap<>();
      placeholders.put("application_url", "http://localhost/test");
      MigrationService migrationService = setUpMigrationService(placeholders, "/db/placeholders/migration");

      migrationService.run();
   }

   @Test
   public void testRepeatableWithPlaceholders() throws SQLException {
      testMigrationWithPlaceholders();

      Map<String, String> placeholders = new HashMap<>();
      placeholders.put("application_url", "http://moved.mydomain/test");
      placeholders.put("sso_enabled", "true");

      MigrationService migrationService = setUpMigrationService(placeholders,
            "/db/placeholders/migration", "/db/placeholders/repeatable");
      migrationService.run();

      Assert.assertEquals("http://moved.mydomain/test", getPreferenceValue("application_url", dataSource));
      Assert.assertEquals("true", getPreferenceValue("sso_enabled", dataSource));
   }

   private MigrationService setUpMigrationService(Map placeholders, String... migrationPaths) {
      return new MigrationService(
            new MigrationConfigurationMock(
                  dataSource,
                  migrationPaths,
                  "/nobaseline",
                  placeholders)
      );
   }

   private String getPreferenceValue(String preferenceName, DataSource dataSource) throws SQLException {
      try (Connection connection = dataSource.getConnection()) {
         try (PreparedStatement statement = connection.prepareStatement(
               "SELECT value FROM test_application_preference WHERE name = ?;")) {
            statement.setEscapeProcessing(true);
            statement.setString(1, preferenceName);
            ResultSet resultSet = statement.executeQuery();
            resultSet.next();
            return resultSet.getString("value");
         }
      }
   }
}
