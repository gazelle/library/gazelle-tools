package net.ihe.gazelle.migration;

import net.ihe.gazelle.migration.application.MigrationConfiguration;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;

class MigrationConfigurationMock implements MigrationConfiguration {

   private final boolean migrationEnabled;
   private final DataSource dataSource;
   private final String[] migrationPaths;
   private final String baselinePath;
   private final Map<String, String> placeholders;

   MigrationConfigurationMock(DataSource dataSource, String[] migrationPaths, String baselinePath) {
      this(true, dataSource, migrationPaths, baselinePath, null);
   }

   public MigrationConfigurationMock(DataSource dataSource, String[] migrationPaths, String baselinePath,
                                     Map<String, String> placeholders) {

      this(true, dataSource, migrationPaths, baselinePath, placeholders);
   }

   MigrationConfigurationMock() {
      this(false, null, null, null, null);
   }

   MigrationConfigurationMock(Boolean migrationEnabled, DataSource dataSource, String[] migrationPaths,
                              String baselinePath, Map<String, String> placeholders) {
      this.migrationEnabled = migrationEnabled;
      this.dataSource = dataSource;
      this.migrationPaths = migrationPaths;
      this.baselinePath = baselinePath;
      this.placeholders = placeholders;
   }

   @Override
   public boolean isMigrationEnabled() {
      return migrationEnabled;
   }

   @Override
   public DataSource getDataSource() {
      return dataSource;
   }

   @Override
   public String[] getMigrationClassPaths() {
      return migrationPaths;
   }

   @Override
   public String getBaselineClassPath() {
      return baselinePath;
   }

   @Override
   public Map<String, String> getPlaceholders() {
      return placeholders;
   }
}
