package net.ihe.gazelle.migration;

import net.ihe.gazelle.java.migration.V6_0_0_5__FileTestMigration;
import net.ihe.gazelle.migration.application.MigrationConfiguration;
import net.ihe.gazelle.migration.application.MigrationException;
import net.ihe.gazelle.migration.application.MigrationService;
import org.apache.commons.io.output.NullOutputStream;
import org.apache.ibatis.jdbc.ScriptRunner;
import org.flywaydb.core.api.FlywayException;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.sql.DataSource;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

public class MigrationTest {

   private static final Logger LOG = LoggerFactory.getLogger(MigrationTest.class);
   public static final String RELATION_DOES_NOT_EXIST_ERROR_CODE = "42P01";

   private DataSource dataSource;

   @Before
   public void setUp() throws SQLException {
      JdbcTestServer.resetDatabase(JdbcTestServer.TEST_DATABASE);
      dataSource = JdbcTestServer.buildLocalDatasource(JdbcTestServer.TEST_DATABASE);
   }

   @After
   public void tearDown() throws SQLException {
      dataSource.getConnection().close();
   }


   @Test
   public void testBaselineMigrationExistingDB() throws SQLException {

      initExistingDatabase();

      MigrationService migrationService = new MigrationService(new MigrationConfigurationMock(dataSource, null, null));
      migrationService.run();

      assertTrue(isMigrationRun(dataSource));
   }

   @Test
   public void testBaselineMigrationEmptyDB() throws SQLException {

      MigrationService migrationService = new MigrationService(new MigrationConfigurationMock(dataSource, null, null));
      migrationService.run();

      assertTrue(isDatabaseInitialized(dataSource));
      assertTrue(isMigrationRun(dataSource));

   }

   /*
    * Test when "baseline" missing, aka 'new application' where schema and init are entirely managed by migrations.
    */
   @Test
   public void testNewApplicationWithoutBaseline() throws SQLException {
      MigrationService migrationService = new MigrationService(
            new MigrationConfigurationMock(dataSource,
                  new String[]{"/db/newapp"},
                  "db/nobaseline"
            )
      );

      migrationService.run();
      assertTrue(isDatabaseInitialized(dataSource));
      assertTrue(isMigrationRun(dataSource));
   }

   @Test
   public void testUpgradeMigration() throws SQLException {
      testBaselineMigrationEmptyDB();

      MigrationService migrationService = new MigrationService(
            new MigrationConfigurationMock(dataSource,
                  new String[]{"/db/migration", "/db/upgrade"},
                  null
            )
      );
      migrationService.run();

      assertTrue(isMigrationUpgraded(dataSource));
   }

   @Test
   public void testJavaMigration() throws SQLException, IOException {
      MigrationService migrationService = new MigrationService(
            new MigrationConfigurationMock(
                  dataSource,
                  new String[]{"/db/migration",
                        "/net/ihe/gazelle/java/migration"},
                  null
            )
      );
      migrationService.run();

      assertTrue(Files.exists(Paths.get(V6_0_0_5__FileTestMigration.FILE_TEST_PATH)));

      Files.delete(Paths.get(V6_0_0_5__FileTestMigration.FILE_TEST_PATH));
   }

   @Test
   public void testCustomBaselinePath() throws SQLException {
      MigrationService migrationService = new MigrationService(
            new MigrationConfigurationMock(dataSource,
                  null,
                  "/db/alternatebaseline"
            )
      );

      migrationService.run();

      assertTrue(isDatabaseInitialized(dataSource));
      assertTrue(isMigrationRun(dataSource));
   }

   @Test(expected = FlywayException.class)
   public void testMigrationFailure() throws SQLException {
      MigrationService migrationService = new MigrationService(
            new MigrationConfigurationMock(dataSource,
                  new String[]{"/db/migration",
                        "/db/failure"},
                  null
            )
      );
      migrationService.run();
   }

   @Test(expected = MigrationException.class)
   public void testBaselineFailure() {
      MigrationService migrationService = new MigrationService(
            new MigrationConfigurationMock(dataSource,
                  new String[]{"/db/migration"},
                  "/db/baselinefailure"
            )
      );
      migrationService.run();
   }

   @Test(expected = MigrationException.class)
   public void testNoMigrationAndNoBaselineError() throws SQLException {
      MigrationConfiguration configuration = new MigrationConfigurationMock(
            dataSource,
            new String[]{"/db/nomigration"},
            "/db/nobaseline"
      );

      MigrationService migrationService = new MigrationService(configuration);
      migrationService.run();
   }

   @Test(expected = IllegalArgumentException.class)
   public void testUndefinedConfigurationError() {
      MigrationService migrationService = new MigrationService(null);
      migrationService.run();
   }

   @Test
   public void testMigrationDisabled() throws SQLException {
      MigrationConfiguration configuration = new MigrationConfigurationMock();
      MigrationService migrationService = new MigrationService(configuration);
      migrationService.run();

      assertFalse(isMigrationRun(dataSource));
   }

   @Test(expected = MigrationException.class)
   public void testDatabaseConnectionError() {
      MigrationConfiguration configuration = new MigrationConfigurationMock(
            JdbcTestServer.buildDatasource("jdbc:postgresql:" + JdbcTestServer.TEST_DATABASE, "unknown", "wrong"),
            new String[]{"/db/migration"},
            "/db/baseline"
      );
      MigrationService migrationService = new MigrationService(configuration);
      migrationService.run();
   }

   private void initExistingDatabase() throws SQLException {
      try (Connection connection = dataSource.getConnection()) {
         runSqlScript(connection, "/db/baseline/schema.sql");
         setDefaultPublicSchema(connection);
         runSqlScript(connection, "/db/baseline/init.sql");
         assertTrue(isDatabaseInitialized(connection));
      }
   }

   private void setDefaultPublicSchema(Connection connection) throws SQLException {
      try (Statement statement = connection.createStatement()) {
         statement.execute("SET search_path = public ;");
      }
   }

   private void runSqlScript(DataSource dataSource, String scriptResourcePath) throws SQLException {
      try (Connection connection = dataSource.getConnection()) {
         runSqlScript(connection, scriptResourcePath);
      }
   }

   private void runSqlScript(Connection connection, String scriptResourcePath) throws SQLException {
      ScriptRunner scriptRunner = new ScriptRunner(connection);
      scriptRunner.setLogWriter(new PrintWriter(new NullOutputStream()));
      scriptRunner.runScript(new InputStreamReader(this.getClass().getResourceAsStream(scriptResourcePath),
            StandardCharsets.UTF_8));
      connection.commit();
   }

   private boolean isMigrationRun(DataSource dataSource) throws SQLException {
      try (Connection connection = dataSource.getConnection()) {
         // Connection must be closed every time to succeed the resetDatabase method.
         try (Statement statement = connection.createStatement()) {
            statement.setEscapeProcessing(true);
            ResultSet resultSet = statement.executeQuery("SELECT last_value FROM mca_doc_type_id_seq;");
            resultSet.next();
            return resultSet.getInt("last_value") > 0;
         } catch (SQLException e) {
            return !RELATION_DOES_NOT_EXIST_ERROR_CODE.equals(e.getSQLState());
         }
      }
   }



   private boolean isMigrationUpgraded(DataSource dataSource) throws SQLException {
      try (Connection connection = dataSource.getConnection()) {
         // Connection must be closed every time to succeed the resetDatabase method.
         try (Statement statement = connection.createStatement()) {
            statement.setEscapeProcessing(true);
            ResultSet resultSet = statement.executeQuery("SELECT count(key) FROM map;");
            resultSet.next();
            return resultSet.getInt("count") >= 0;
         } catch (SQLException e) {
            return !e.getMessage().contains("relation \"map\" does not exist");
         }
      }
   }

   private boolean isDatabaseInitialized(DataSource dataSource) throws SQLException {
      try (Connection connection = dataSource.getConnection()) {
         return isDatabaseInitialized(connection);
      }
   }

   private boolean isDatabaseInitialized(Connection connection) throws SQLException {
      try (Statement statement = connection.createStatement()) {
         try (ResultSet resultSet = statement.executeQuery("SELECT count(*) FROM cmn_application_preference;")) {
            resultSet.next();
            return resultSet.getInt("count") > 0;
         }
      }
   }

}
