package net.ihe.gazelle.migration;

import org.postgresql.ds.PGSimpleDataSource;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;

/**
 * PostgreSQL specific.
 */
class JdbcTestServer {

   static final String TEST_DATABASE = "migration-test";
   static final String ADMIN_DATABASE = "postgres";

   // FIXME make connection information modifiable from maven pom.xml and/or infra environment.
   static DataSource buildLocalDatasource(String databaseName) {
      String dbHost = System.getProperty("it.jdbc.db.host", "localhost");
      return buildDatasource("jdbc:postgresql://"+dbHost+":5432/" + databaseName, "gazelle", "gazelle");
   }

   static DataSource buildDatasource(String url, String user, String password) {
      PGSimpleDataSource dataSource = new PGSimpleDataSource();
      dataSource.setURL(url);
      dataSource.setUser(user);
      dataSource.setPassword(password);
      return dataSource;
   }

   static void resetDatabase(String databaseName) throws SQLException {
      DataSource adminDataSource = buildLocalDatasource(ADMIN_DATABASE);
      try (Connection connection = adminDataSource.getConnection()) {
         connection.createStatement().execute("DROP DATABASE IF EXISTS \"" + databaseName + "\" ;");
         connection.createStatement().execute("CREATE DATABASE \"" + databaseName + "\" OWNER gazelle ENCODING=UTF8;");
         if (!connection.getAutoCommit()) {
            connection.commit();
         }
      }
   }
}
