package net.ihe.gazelle.model4test;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.util.Objects;

/**
 * Fake entity for migration tests
 */
@Entity
@Table(name = "test_test_step_run")
@SequenceGenerator(name = "TestStepRunSequence", sequenceName = "test_test_step_run_id_seq", allocationSize = 1)
public class TestStepRun {

   public enum StepResult {
      PASSED,
      FAILED,
      DONE,
      ERROR,
      SKIPPED;
   }

   @Id
   @Column(name = "id")
   @GeneratedValue(generator = "TestStepRunSequence", strategy = GenerationType.SEQUENCE)
   private Integer id;

   @Column(name = "step_id", nullable = false)
   private String stepId;

   @ManyToOne(optional = false)
   @JoinColumn(name = "test_run_id", nullable = false)
   private TestRun testRun;

   @Enumerated(EnumType.STRING)
   @Column(name = "result")
   private StepResult result;

   public Integer getId() {
      return id;
   }

   public void setId(Integer id) {
      this.id = id;
   }

   public String getStepId() {
      return stepId;
   }

   public void setStepId(String stepId) {
      this.stepId = stepId;
   }

   public TestRun getTestRun() {
      return testRun;
   }

   public void setTestRun(TestRun testRun) {
      this.testRun = testRun;
   }

   public StepResult getResult() {
      return result;
   }

   public void setResult(StepResult result) {
      this.result = result;
   }

   @Override
   public boolean equals(Object o) {
      if (this == o) {
         return true;
      }
      if (!(o instanceof TestStepRun)) {
         return false;
      }
      TestStepRun that = (TestStepRun) o;
      return Objects.equals(stepId, that.stepId) && Objects.equals(testRun,
            that.testRun) && result == that.result;
   }

   @Override
   public int hashCode() {
      return Objects.hash(stepId, testRun, result);
   }
}
