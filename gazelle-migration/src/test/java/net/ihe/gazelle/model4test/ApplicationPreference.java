package net.ihe.gazelle.model4test;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * Fake entity for migration tests
 */
@Entity
@Table(name = "test_application_preference", schema = "public")
public class ApplicationPreference implements Serializable {
   
   private static final long serialVersionUID = -5994534457643398088L;

   @Id
   @Column(name = "name", length = 127)
   private String name;

   @Column(name = "class")
   private String className;

   @Column(name = "value")
   private String value;

   @Column(name = "description", length = 511)
   private String description;

   public String getName() {
      return name;
   }

   public void setName(String name) {
      this.name = name;
   }

   public String getValue() {
      return value;
   }

   public void setValue(String value) {
      this.value = value;
   }

   public String getDescription() {
      return description;
   }

   public void setDescription(String description) {
      this.description = description;
   }

   public String getClassName() {
      return className;
   }

   public void setClassName(String className) {
      this.className = className;
   }

   @Override
   public boolean equals(Object o) {
      if (this == o) {
         return true;
      }
      if (!(o instanceof ApplicationPreference)) {
         return false;
      }
      ApplicationPreference that = (ApplicationPreference) o;
      return name.equals(that.name) && Objects.equals(value,
            that.value) && Objects.equals(description, that.description) && Objects.equals(
            className, that.className);
   }

   @Override
   public int hashCode() {
      return Objects.hash(name, value, description, className);
   }
}
