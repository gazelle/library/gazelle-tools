package net.ihe.gazelle.java.migration;

import org.flywaydb.core.api.migration.jdbc.JdbcMigration;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Connection;

public class V6_0_0_5__FileTestMigration implements JdbcMigration {

   public static final String FILE_TEST_PATH = "/tmp/javaMigrateTest.txt";

   @Override
   public void migrate(Connection connection) throws Exception {

      Path filePath = Files.createFile(Paths.get(FILE_TEST_PATH));

   }
}
