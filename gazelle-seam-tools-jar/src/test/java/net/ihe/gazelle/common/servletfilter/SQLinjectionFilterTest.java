package net.ihe.gazelle.common.servletfilter;

import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class SQLinjectionFilterTest {

    @Test
    public void test_null_character_injections() {
        assertTrue(SQLinjectionFilter.isUnsafe("\0"));
        assertTrue(SQLinjectionFilter.isUnsafe("testing\0hello"));
    }

    @Test
    public void test_select_character_injections() {
        assertTrue(SQLinjectionFilter.isUnsafe("'select"));
        assertTrue(SQLinjectionFilter.isUnsafe("testing'select"));
    }

    @Test
    public void test_SQL_injections() {
        assertTrue(SQLinjectionFilter
                .isUnsafe("http://www.mydomain.com/products/products.asp?productid=123 'SELECT user-name, password FROM USERS"));
        assertTrue(SQLinjectionFilter
                .isUnsafe("http://www.mydomain.com/products/products.asp?productid=123; 'DROP TABLE Products"));
    }
}
