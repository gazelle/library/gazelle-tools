/*
 * Copyright 2016 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.ihe.gazelle.common.util;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * <b>Class Description : </b>XmlFormatterTest<br>
 * <br>
 *
 * @author Jean-Francois Labbé / IHE-Europe development Project
 * @version 1.0 - 20/04/16
 * @class XmlFormatterTest
 * @package net.ihe.gazelle.common.util

 */
public class XmlFormatterTest {
    @Test
    public void format() throws Exception {
        String xml = "<xml><var>gazelle</var></xml>";
        String expected = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
                "\n" +
                "<xml>\n" +
                "  <var>gazelle</var>\n" +
                "</xml>\n";
        assertEquals(expected, XmlFormatter.format(xml));
    }
}