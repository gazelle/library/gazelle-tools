/*******************************************************************************
 * Copyright 2011 IHE International (http://www.ihe.net)
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package net.ihe.gazelle.common.filter;

import net.ihe.gazelle.common.filter.criterion.ValueFormatter;
import net.ihe.gazelle.hql.criterion.HQLCriterion;
import org.apache.commons.lang.StringUtils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Suggester<E, F> implements Serializable {

    private static final long serialVersionUID = -384399847740065090L;
    public static final int ABBREVIATION_LENGTH = 50;
    private Filter<E> filter;
    private String keyword;
    private HQLCriterion<E, F> criterion;

    public Suggester(Filter<E> filter, String keyword,
                     HQLCriterion<E, F> criterion) {
        super();
        this.filter = filter;
        this.keyword = keyword;
        this.criterion = criterion;
    }

    @SuppressWarnings("unchecked")
    public List<F> suggest(String prefix) {
        List<F> allValues = (List<F>) filter.getPossibleValues(keyword);
        List<F> values = allValues;
        if ((prefix != null) && prefix.startsWith(" ")) {
            prefix = prefix.substring(1);
        }
        // show all items if something is already selected and the label match
        // the selected item
        boolean showAllItems = true;

        if ((allValues != null) && (prefix != null) && (prefix.length() > 1)) {
            showAllItems = false;
        }
        F selectedValue = (F) filter.getFilterValues().get(keyword);
        if (selectedValue != null) {
            String label = Filter.getSelectableLabel(criterion, selectedValue);
            label = StringUtils.abbreviate(label, ABBREVIATION_LENGTH);
            if ((label != null) && (prefix != null)
                    && label.equalsIgnoreCase(prefix)) {
                showAllItems = true;
            }
        }
        if (!showAllItems) {
            prefix = prefix.toLowerCase();
            values = new ArrayList<F>();
            for (F e : allValues) {
                String label = Filter.getSelectableLabel(criterion, e);
                if (label != null) {
                    label = label.toLowerCase();
                    if (label.contains(prefix)) {
                        values.add(e);
                    }
                }
            }
        }
        return values;
    }

    public F find(String prefix) {
        List<F> allValues = (List<F>) filter.getPossibleValues(keyword);

        for (F e : allValues) {
            String label = Filter.getSelectableLabel(criterion, e);
            if (label != null && label.equals(prefix)) {
                return e;
            }
        }

        return null;
    }
}
