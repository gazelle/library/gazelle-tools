package net.ihe.gazelle.common.tag;

import java.io.IOException;
import java.util.Date;

import javax.faces.component.UIComponentBase;
import javax.faces.context.FacesContext;
import javax.faces.context.ResponseWriter;

import net.ihe.gazelle.common.util.DateDisplayUtil;

import org.apache.commons.lang.StringEscapeUtils;

public class DateComponent extends UIComponentBase {

	protected boolean getAttributeValueBoolean(String attributeName, boolean defaultValue) {
		Object detailed = getAttributes().get(attributeName);
		boolean result = defaultValue;
		if (detailed != null) {
			if (detailed instanceof Boolean) {
				result = (Boolean) detailed;
			} else if (detailed instanceof String) {
				result = Boolean.valueOf((String) detailed);
			}
		}
		return result;
	}

	@Override
	public String getFamily() {
		return "gazelle-date";
	}

	@Override
	public void encodeBegin(FacesContext context) throws IOException {
		ResponseWriter writer = context.getResponseWriter();

		Object value = getAttributes().get("value");
		boolean showDate = getAttributeValueBoolean("date", true);
		boolean showTime = getAttributeValueBoolean("time", true);
		boolean showTooltip = getAttributeValueBoolean("tooltip", false);

		if ((value != null) && (value instanceof Date)) {
			Date date = (Date) value;

			if (showTooltip) {
				writer.write(DateDisplayUtil.displayDateTimeHTML(date));
			} else {
				if (showDate && !showTime) {
					StringEscapeUtils.escapeHtml(writer, DateDisplayUtil.displayDate(date));
				} else if (!showDate && showTime) {
					StringEscapeUtils.escapeHtml(writer, DateDisplayUtil.displayTime(date));
				} else {
					StringEscapeUtils.escapeHtml(writer, DateDisplayUtil.displayDateTime(date));
				}
			}

		}
		writer.flush();
	}

	@Override
	public void encodeEnd(FacesContext context) throws IOException {
		if (isRendered()) {
			return;
		}
	}

}
