package net.ihe.gazelle.common.util;

import java.util.Date;

import net.ihe.gazelle.dates.DateDisplayer;

public class DateDisplayerProvider implements DateDisplayer {

	@Override
	public Object displayDateTime(Date date) {
		return DateDisplayUtil.displayDateTime(date);
	}

}
