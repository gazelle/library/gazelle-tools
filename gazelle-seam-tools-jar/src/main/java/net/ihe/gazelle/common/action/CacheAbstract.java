package net.ihe.gazelle.common.action;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class CacheAbstract implements CacheLocal {

    private static Logger log = LoggerFactory.getLogger(CacheAbstract.class);

    public static final class NullValue {

        @Override
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            return true;
        }
    }

    ;

    private static final NullValue NULL_VALUE = new NullValue();

    @Override
    public Object getValueUpdater(String key, CacheUpdater cacheUpdater, Object parameter) {
        Object result = getValue(key);
        if (result == null) {
            result = cacheUpdater.getValue(key, parameter);
            if (result == null) {
                result = NULL_VALUE;
            }
            setValue(key, result);
        }

        if (NULL_VALUE.equals(result)) {
            return null;
        }

        return result;
    }
}
