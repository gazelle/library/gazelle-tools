package net.ihe.gazelle.common.filter.list;

import net.ihe.gazelle.common.filter.NoHQLQueryBuilder;
import net.ihe.gazelle.hql.HQLRestriction;
import net.ihe.gazelle.hql.beans.HQLStatistic;
import net.ihe.gazelle.hql.beans.HQLStatisticItem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ListQueryBuilder<T> implements NoHQLQueryBuilder<T> {

    private static final Logger LOGGER = LoggerFactory.getLogger(ListQueryBuilder.class);

    private final List<T> listElements;

    private List<T> listElementsUpdatedElements;

    private final Map<String, String> mapPropVal = new HashMap<String, String>();

    private final Map<String, Object> mapPropEq = new HashMap<>();

    private final Map<String, Boolean> mapOrder = new HashMap<String, Boolean>();

    private int firstResult = 0;
    private int maxResults = 0;

    private boolean cachable = true;

    public ListQueryBuilder(List<T> listElements) {
        this.listElements = copyList(listElements);
        this.listElementsUpdatedElements = copyList(listElements);
    }

    private List<T> copyList(List<T> listElements2) {
        List<T> res = new ArrayList<T>();
        if (listElements2 != null) {
            for (T t : listElements2) {
                res.add(t);
            }
        }
        return res;
    }

    @Override
    public int getCount() {
        getList();
        return listElementsUpdatedElements != null ? listElementsUpdatedElements.size() : 0;
    }

    @Override
    public List<T> getList() {
        listElementsUpdatedElements = this.copyList(this.listElements);
        if (mapPropEq != null) {
            for (Map.Entry<String, Object> prop : mapPropEq.entrySet()) {
                if ("_id".equals(prop.getKey())) {
                    listElementsUpdatedElements.clear();
                    listElementsUpdatedElements.add(this.listElements.get((Integer) prop.getValue()));
                    return listElementsUpdatedElements;
                }
                for (T el : listElements) {
                    if (!checkIfElementHasAPropertyWithEqualValue(el, prop.getKey(), prop.getValue().toString())) {
                        listElementsUpdatedElements.remove(el);
                    }
                }
            }
        }

        if (mapPropVal != null) {
            for (Map.Entry<String, String> prop : mapPropVal.entrySet()) {
                for (T el : listElements) {
                    if (!checkIfElementHasAPropertyWithALikeValue(el, prop.getKey(), prop.getValue())) {
                        listElementsUpdatedElements.remove(el);
                    }
                }
            }
        }

        if (mapOrder != null) {
            SortDynamic<T> sd = new SortDynamic<T>();
            this.listElementsUpdatedElements = sd.sortList(this.listElementsUpdatedElements, this.mapOrder);
        }
        if (listElementsUpdatedElements.size() < firstResult || firstResult < 0) {
            firstResult = 0;
        }
        int lastResult = 0;
        if (maxResults == 0) {
            lastResult = listElementsUpdatedElements.size();
        } else {
            lastResult = (firstResult + maxResults) < listElementsUpdatedElements.size() ? (firstResult + maxResults) : listElementsUpdatedElements.size();
        }
        return listElementsUpdatedElements.subList(firstResult, lastResult);
    }

    private boolean checkIfElementHasAPropertyWithEqualValue(T el, String prop, String equal) {
        String valueOfElementProperty = FieldProvider.getElementPropertyValueAsString(el, prop);
        if (valueOfElementProperty != null && equal != null && !equal.trim().equals("")) {
            return valueOfElementProperty.equals(equal);
        }
        return true;
    }

    private boolean checkIfElementHasAPropertyWithALikeValue(T el, String prop, String contain) {
        String valueOfElementProperty = FieldProvider.getElementPropertyValueAsString(el, prop);
        String lowerCaseProperty = (valueOfElementProperty != null) ? valueOfElementProperty.toLowerCase() : null;
        String lowerCaseContain = (contain != null) ? contain.toLowerCase() : null;
        if (lowerCaseProperty != null && lowerCaseContain != null && !lowerCaseContain.trim().isEmpty()) {
            return lowerCaseProperty.contains(lowerCaseContain);
        }
        return true;
    }

    @Override
    public List<T> getListNullIfEmpty() {
        List<T> result = getList();
        if (result.size() == 0) {
            return null;
        } else {
            return result;
        }
    }

    @Override
    public T getUniqueResult() {
        T result = null;
        List<T> list = getList();
        if (list != null && list.size() > 0) {
            result = list.get(0);
        } else {
            result = null;
        }
        return result;
    }

    @Override
    public List<HQLStatistic<T>> getListWithStatistics(
            List<HQLStatisticItem> items) {
        // -- ignore
        return null;
    }

    @Override
    public List<Object> getListWithStatisticsItems(
            List<HQLStatisticItem> items, HQLStatistic<T> item,
            int statisticItemIndex) {
        // --ignore
        return null;
    }

    @Override
    public void setFirstResult(int firstResult) {
        this.firstResult = firstResult;
    }

    @Override
    public void setMaxResults(int maxResults) {
        this.maxResults = maxResults;
    }

    @Override
    public boolean isCachable() {
        return cachable;
    }

    @Override
    public void setCachable(boolean cachable) {
        this.cachable = cachable;
    }

    @Override
    public void addRestriction(HQLRestriction restriction) {
        throw new UnsupportedOperationException("Use addLike or addEq methods instead.");
    }

    @Override
    public void addLike(String propertyName, String filterValue) {
        this.mapPropVal.put(propertyName, filterValue);
    }

    @Override
    public void addOrder(String propertyName, boolean equals) {
        this.mapOrder.put(propertyName, equals);
    }

    @Override
    public void addEq(String string, Object dataItem) {
        this.mapPropEq.put(string, dataItem);
    }

    @Override
    public Object getId(T t) {
        if (this.listElements != null) {
            int i = 0;
            for (T el : this.listElements) {
                if (el == t) {
                    return Integer.valueOf(i);
                }
                i++;
            }
        }
        return null;
    }

    @Override
    public Map<String, Long> getPossibleCountValues(String propertyName) {
        return null;
    }
}
