package net.ihe.gazelle.common.util;

import javax.faces.context.FacesContext;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class GazelleCookie {

    //Max age of cookie to 1 month
    private static final int MAX_AGE = 60 * 60 * 24 * 30;

    private GazelleCookie() {
        super();
    }

    public static void setCookie(String cookieId, String value) {
        HttpServletResponse response = getHttpServletResponse();
        setCookie(response, cookieId, value);
    }

    public static void setCookie(HttpServletResponse response, String cookieId, String value) {
        setCookie(response, cookieId, value, "/", MAX_AGE);
    }

    public static void setCookie(String cookieId, String value, String path, int maxAge) {
        HttpServletResponse response = getHttpServletResponse();
        setCookie(response, cookieId, value, path, maxAge);
    }

    public static void setCookie(HttpServletResponse response, String cookieId, String value, String path, int maxAge) {
        Cookie cookie = new Cookie(cookieId, value);
        cookie.setPath(path);
        cookie.setMaxAge(maxAge);
        response.addCookie(cookie);
    }

    private static HttpServletResponse getHttpServletResponse() {
        FacesContext context = FacesContext.getCurrentInstance();
        return (HttpServletResponse) context.getExternalContext().getResponse();
    }


    public static String getCookie(String cookieId) {
        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
        return getCookie(request, cookieId);
    }

    public static String getCookie(HttpServletRequest request, String cookieId) {
        Cookie[] cookies = request.getCookies();
        String result = null;
        if (cookies != null) {
            for (Cookie cookie : cookies) {
                if (cookie.getName().equals(cookieId)) {
                    result = cookie.getValue();
                }
            }
        }
        return result;
    }

    public static void deleteCookie(String cookieId, String value, String cookiePath) {
        deleteCookie(getHttpServletResponse(), cookieId, value, cookiePath);
    }


    public static void deleteCookie(HttpServletResponse response, String cookieId, String value, String cookiePath) {
        Cookie cookie = new Cookie(cookieId, value);
        cookie.setPath(cookiePath);
        cookie.setMaxAge(0);
        response.addCookie(cookie);
    }

}
