package net.ihe.gazelle.common.servletfilter;

import net.ihe.gazelle.util.Md5Encryption;
import org.apache.commons.io.IOUtils;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Install;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.intercept.BypassInterceptors;
import org.jboss.seam.annotations.web.Filter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.activation.FileTypeMap;
import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Scope(ScopeType.APPLICATION)
@Name("net.ihe.gazelle.common.servletfilter.StaticFilter")
@Install
@BypassInterceptors
@Filter
public class StaticFilter implements javax.servlet.Filter {

    private static Logger log = LoggerFactory.getLogger(StaticFilter.class);
    private static Map<String, String> CHECKSUMS = Collections.synchronizedMap(new HashMap<String, String>());
    // 1 day : 24 * 60 * 60 * 1000
    private static final long OFFSET_EXPIRES = 51000000l;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        // NOTHING
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException,
            ServletException {

        if (request instanceof HttpServletRequest) {
            HttpServletRequest httpRequest = (HttpServletRequest) request;
            HttpServletResponse httpResponse = (HttpServletResponse) response;

            String resource = "/static" + httpRequest.getServletPath();

            InputStream inputStream = StaticFilter.class.getResourceAsStream(resource);

            if (inputStream == null) {
                chain.doFilter(request, response);
            } else {

                String checksum = CHECKSUMS.get(resource);
                if (checksum == null) {
                    InputStream inputStreamFromChecksum = StaticFilter.class.getResourceAsStream(resource);
                    try {
                        checksum = Md5Encryption.calculateMD5ChecksumForInputStream(inputStreamFromChecksum);
                        inputStreamFromChecksum.close();
                    } catch (Exception e) {
                        checksum = null;
                        log.error("Failed to compute MD5 for " + resource, e);
                    }
                    CHECKSUMS.put(resource, checksum);
                }

                String resourceName = resource.toLowerCase();
                if ((resource != null) && resourceName.endsWith(".pdf")) {
                    httpResponse.setContentType("application/pdf");
                } else if ((resource != null) && resourceName.endsWith(".css")) {
                    httpResponse.setContentType("text/css");
                } else if ((resource != null) && resourceName.endsWith(".js")) {
                    httpResponse.setContentType("text/javascript");
                } else {
                    String contentType = FileTypeMap.getDefaultFileTypeMap().getContentType(resource);
                    httpResponse.setContentType(contentType);
                }

                if (checksum != null) {
                    long expires = new Date().getTime() + OFFSET_EXPIRES;
                    httpResponse.setDateHeader("Expires", expires);
                    httpResponse.setHeader("ETag", checksum);
                    httpResponse.setHeader("Cache-Control", "must-revalidate");
                    String eTag = httpRequest.getHeader("If-None-Match");
                    if (checksum.equals(eTag)) {
                        httpResponse.setStatus(HttpServletResponse.SC_NOT_MODIFIED);
                        inputStream.close();
                        return;
                    }
                }

                OutputStream outputStream = response.getOutputStream();
                IOUtils.copyLarge(inputStream, outputStream);
                inputStream.close();
                outputStream.close();
            }
        } else {
            chain.doFilter(request, response);
        }
    }

    @Override
    public void destroy() {
        // NOTHING
    }
}
