package net.ihe.gazelle.common.bootstrapComponent;

/**
 * Created by jlabbe on 22/07/15.
 */

import org.richfaces.renderkit.html.PopupPanelBaseRenderer;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.context.ResponseWriter;
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;

import static org.richfaces.renderkit.RenderKitUtils.*;


public class ModalPanel extends PopupPanelBaseRenderer {
    private static final Attributes PASS_THROUGH_ATTRIBUTES17 = attributes()
            .generic("align", "align")


            .generic("dir", "dir")


            .generic("lang", "lang")


            .generic("onclick", "onclick")


            .generic("ondblclick", "ondblclick")


            .generic("onkeydown", "onkeydown")


            .generic("onkeypress", "onkeypress")


            .generic("onkeyup", "onkeyup")


            .generic("onmousedown", "onmousedown")


            .generic("onmousemove", "onmousemove")


            .generic("onmouseout", "onmouseout")


            .generic("onmouseover", "onmouseover")


            .generic("onmouseup", "onmouseup")


            .generic("role", "role")


            .generic("title", "title");

    public static final int Z_INDEX = 100;
    private static final Attributes ATTRIBUTES_FOR_SCRIPT_HASH18 = attributes()
            .generic("width", "width")
            .defaultValue(-1)

            .generic("height", "height")
            .defaultValue(-1)

            .generic("minWidth", "minWidth")
            .defaultValue(-1)

            .generic("minHeight", "minHeight")
            .defaultValue(-1)

            .generic("maxWidth", "maxWidth")
            .defaultValue(Integer.MIN_VALUE)

            .generic("maxHeight", "maxHeight")
            .defaultValue(Integer.MIN_VALUE)

            .generic("moveable", "moveable")
            .defaultValue(true)

            .generic("followByScroll", "followByScroll")
            .defaultValue(true)

            .generic("left", "left")


            .generic("top", "top")


            .generic("zindex", "zindex")
            .defaultValue(Z_INDEX)

            .generic("shadowDepth", "shadowDepth")


            .generic("shadowOpacity", "shadowOpacity")


            .generic("domElementAttachment", "domElementAttachment")


            .generic("show", "show")
            .defaultValue(false)

            .generic("keepVisualState", "keepVisualState")
            .defaultValue(false)

            .generic("autosized", "autosized")
            .defaultValue(false)

            .generic("resizeable", "resizeable")
            .defaultValue(false)

            .generic("modal", "modal")
            .defaultValue(true)

            .generic("overlapEmbedObjects", "overlapEmbedObjects")
            .defaultValue(false)

            .generic("visualOptions", "visualOptions");

    private static final Attributes ATTRIBUTES_FOR_SCRIPT_HASH19 = attributes()
            .generic("onshow", "onshow", "show")


            .generic("onhide", "onhide", "hide")


            .generic("onresize", "onresize", "resize")


            .generic("onmove", "onmove", "move")


            .generic("onbeforeshow", "onbeforeshow", "beforeshow")


            .generic("onbeforehide", "onbeforehide", "beforehide");


    private static String convertToString(Object object) {
        return object != null ? object.toString() : "";
    }

    private static boolean convertToBoolean(Object object) {
        if (object == null) {
            return false;
        }

        if (object instanceof Boolean) {
            return (Boolean) object;
        }

        return Boolean.valueOf(object.toString());
    }

    private static boolean isEqual(Object o1, Object o2) {
        if (o1 != null) {
            return o1.equals(o2);
        } else {
            //o1 == null
            return o2 == null;
        }
    }

    @Override
    public void doEncodeEnd(ResponseWriter responseWriter, FacesContext facesContext, UIComponent component)
            throws IOException {
        String clientId = component.getClientId(facesContext);
        checkOptions(facesContext, component);
        responseWriter.startElement("div", component);
        {
            String value = clientId;
            if (null != value &&
                    value.length() > 0
                    ) {
                responseWriter.writeAttribute("id", value, null);
            }

        }

        responseWriter.writeAttribute("style", "visibility: hidden;", null);

        responseWriter.startElement("div", component);
        {
            String value = "modal-dialog" + convertToString(component.getAttributes().get("styleClass"));
            if (null != value &&
                    value.length() > 0
                    ) {
                responseWriter.writeAttribute("class", value, null);
            }

        }

        {
            String value = convertToString(clientId) + "_container";
            if (null != value &&
                    value.length() > 0
                    ) {
                responseWriter.writeAttribute("id", value, null);
            }

        }

        {
            String value = this.getContainerStyle(component) + " display: block;";
            responseWriter.writeAttribute("style", value, null);
        }


        renderPassThroughAttributes(facesContext, component,
                PASS_THROUGH_ATTRIBUTES17);

        responseWriter.startElement("div", component);
        responseWriter.writeAttribute("class", "modal-content", null);

        if (((component.getFacet("header") != null) && component.getFacet("header").isRendered())) {
            responseWriter.startElement("div", component);
            {
                String value = "modal-header" + convertToString(component.getAttributes().get("headerClass"));
                if (null != value &&
                        value.length() > 0
                        ) {
                    responseWriter.writeAttribute("class", value, null);
                }

            }

            {
                String value = convertToString(clientId) + "_header";
                if (null != value &&
                        value.length() > 0
                        ) {
                    responseWriter.writeAttribute("id", value, null);
                }

            }

            responseWriter.startElement("button", component);
            responseWriter.writeAttribute("type", "button", null);
            responseWriter.writeAttribute("class", "close", null);
            responseWriter.writeAttribute("data-dismiss", "modal", null);
            responseWriter.writeAttribute("aria-label", "Close", null);
            responseWriter.startElement("span", component);
            responseWriter.writeAttribute("aria-hidden", "true", null);
            responseWriter.startElement("span", component);
            responseWriter.writeAttribute("href", "#", null);
            responseWriter.writeAttribute("onclick", "RichFaces.$('" + clientId + "').hide()", null);
            responseWriter.writeAttribute("style", "cursor: pointer;", null);
            responseWriter.write("X");
            responseWriter.endElement("span");
            responseWriter.endElement("span");
            responseWriter.endElement("button");

            responseWriter.startElement("h4", component);
            responseWriter.writeAttribute("class", "modal-title", null);
            renderHeaderFacet(facesContext, component);
            responseWriter.endElement("h4");
            responseWriter.endElement("div");
        }
        if (((component.getAttributes().get("header") != null) && ((component.getFacet("header") == null) || (!component.getFacet("header").isRendered())))) {
            responseWriter.startElement("div", component);
            {
                String value = "rf-pp-hdr " + convertToString(component.getAttributes().get("headerClass"));
                if (null != value &&
                        value.length() > 0
                        ) {
                    responseWriter.writeAttribute("class", value, null);
                }

            }

            {
                String value = convertToString(clientId) + "_header";
                if (null != value &&
                        value.length() > 0
                        ) {
                    responseWriter.writeAttribute("id", value, null);
                }

            }


            responseWriter.startElement("div", component);
            responseWriter.writeAttribute("class", "rf-pp-hdr-cnt", null);

            {
                String value = convertToString(clientId) + "_header_content";
                if (null != value &&
                        value.length() > 0
                        ) {
                    responseWriter.writeAttribute("id", value, null);
                }

            }


            {
                Object text = component.getAttributes().get("header");
                if (text != null) {
                    responseWriter.writeText(text, null);
                }
            }

            responseWriter.endElement("div");
            responseWriter.endElement("div");
        }
        responseWriter.startElement("div", component);
        responseWriter.writeAttribute("class", "modal-body", null);

        {
            String value = convertToString(clientId) + "_content_scroller";
            if (null != value &&
                    value.length() > 0
                    ) {
                responseWriter.writeAttribute("id", value, null);
            }

        }


        renderChildren(facesContext, component);
        responseWriter.endElement("div");
        if (convertToBoolean(component.getAttributes().get("resizeable"))) {
            responseWriter.startElement("div", component);
            responseWriter.writeAttribute("class", "rf-pp-hndlr  rf-pp-hndlr-l", null);

            {
                String value = convertToString(clientId) + "ResizerW";
                if (null != value &&
                        value.length() > 0
                        ) {
                    responseWriter.writeAttribute("id", value, null);
                }

            }


            responseWriter.endElement("div");
            responseWriter.startElement("div", component);
            responseWriter.writeAttribute("class", "rf-pp-hndlr rf-pp-hndlr-r", null);

            {
                String value = convertToString(clientId) + "ResizerE";
                if (null != value &&
                        value.length() > 0
                        ) {
                    responseWriter.writeAttribute("id", value, null);
                }

            }


            responseWriter.endElement("div");
            responseWriter.startElement("div", component);
            responseWriter.writeAttribute("class", "rf-pp-hndlr rf-pp-hndlr-t", null);

            {
                String value = convertToString(clientId) + "ResizerN";
                if (null != value &&
                        value.length() > 0
                        ) {
                    responseWriter.writeAttribute("id", value, null);
                }

            }


            responseWriter.endElement("div");
            responseWriter.startElement("div", component);
            responseWriter.writeAttribute("class", "rf-pp-hndlr rf-pp-hndlr-b", null);

            {
                String value = convertToString(clientId) + "ResizerS";
                if (null != value &&
                        value.length() > 0
                        ) {
                    responseWriter.writeAttribute("id", value, null);
                }

            }


            responseWriter.endElement("div");
            responseWriter.startElement("div", component);
            responseWriter.writeAttribute("class", "rf-pp-hndlr rf-pp-hndlr-tl", null);

            {
                String value = convertToString(clientId) + "ResizerNW";
                if (null != value &&
                        value.length() > 0
                        ) {
                    responseWriter.writeAttribute("id", value, null);
                }

            }


            responseWriter.endElement("div");
            responseWriter.startElement("div", component);
            responseWriter.writeAttribute("class", "rf-pp-hndlr rf-pp-hndlr-tr", null);

            {
                String value = convertToString(clientId) + "ResizerNE";
                if (null != value &&
                        value.length() > 0
                        ) {
                    responseWriter.writeAttribute("id", value, null);
                }

            }


            responseWriter.endElement("div");
            responseWriter.startElement("div", component);
            responseWriter.writeAttribute("class", "rf-pp-hndlr rf-pp-hndlr-bl", null);

            {
                String value = convertToString(clientId) + "ResizerSW";
                if (null != value &&
                        value.length() > 0
                        ) {
                    responseWriter.writeAttribute("id", value, null);
                }

            }


            responseWriter.endElement("div");
            responseWriter.startElement("div", component);
            responseWriter.writeAttribute("class", "rf-pp-hndlr rf-pp-hndlr-br", null);

            {
                String value = convertToString(clientId) + "ResizerSE";
                if (null != value &&
                        value.length() > 0
                        ) {
                    responseWriter.writeAttribute("id", value, null);
                }

            }


            responseWriter.endElement("div");
        }

        responseWriter.endElement("div");
        responseWriter.endElement("div");
        Map<String, Object> options = new LinkedHashMap<String, Object>();
        addToScriptHash(options, "left", this.getLeftOrDefault(component), "auto", null);

        addToScriptHash(options, "top", this.getTopOrDefault(component), "auto", null);


        addToScriptHash(options, facesContext, component, ATTRIBUTES_FOR_SCRIPT_HASH18, null);


        addToScriptHash(options, facesContext, component, ATTRIBUTES_FOR_SCRIPT_HASH19, ScriptHashVariableWrapper.eventHandler);

        responseWriter.startElement("script", component);
        responseWriter.writeAttribute("type", "text/javascript", null);


        {
            Object text = "new RichFaces.ui.PopupPanel(" + convertToString(toScriptArgs(clientId, options)) + ");";
            if (text != null) {
                responseWriter.writeText(text, null);
            }
        }

        if (!isEqual(component.getAttributes().get("show"), false)) {
            {
                Object text = "RichFaces.ui.PopupPanel.showPopupPanel(" + convertToString(toScriptArgs(clientId, this.getHandledVisualOptions(component))) + ");";
                if (text != null) {
                    responseWriter.writeText(text, null);
                }
            }

        }

        responseWriter.endElement("script");
        responseWriter.endElement("div");

    }

}

