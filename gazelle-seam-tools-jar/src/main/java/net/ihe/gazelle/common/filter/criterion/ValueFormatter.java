package net.ihe.gazelle.common.filter.criterion;

public interface ValueFormatter {

    String format(Object o);
    String formatLabel(Object o);
    String formatCount(Object o);
}
