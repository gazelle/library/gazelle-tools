package net.ihe.gazelle.common.filter.list;

import net.ihe.gazelle.common.filter.NoHQLQueryBuilder;
import net.ihe.gazelle.common.util.DataScrollerMemory;
import org.ajax4jsf.model.DataVisitResult;
import org.ajax4jsf.model.DataVisitor;
import org.ajax4jsf.model.ExtendedDataModel;
import org.ajax4jsf.model.Range;
import org.ajax4jsf.model.SequenceRange;
import org.apache.commons.collections.BidiMap;
import org.apache.commons.collections.bidimap.DualHashBidiMap;
import org.jboss.seam.Component;
import org.richfaces.component.SortOrder;
import org.richfaces.model.Arrangeable;
import org.richfaces.model.ArrangeableState;
import org.richfaces.model.FilterField;
import org.richfaces.model.SortField;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.el.ELException;
import javax.el.Expression;
import javax.el.ValueExpression;
import javax.faces.FacesException;
import javax.faces.context.FacesContext;
import java.io.Serializable;
import java.lang.ref.WeakReference;
import java.util.List;

public abstract class NoHQLDataModel<T> extends ExtendedDataModel<T>
      implements Arrangeable, Serializable {

   private static final Logger LOGGER = LoggerFactory.getLogger(NoHQLDataModel.class);
   private static final long serialVersionUID = 8773031927238386576L;
   private final String dataTableId;
   private transient SequenceRange cachedRange;
   private transient ArrangeableState arrangeableState;
   private transient NoHQLQueryBuilder<T> queryBuilder;
   private transient WeakReference<List<T>> weakCache = new WeakReference<List<T>>(null);
   private final transient BidiMap indexKeys = new DualHashBidiMap();
   private int rowIndex = -1;

   protected NoHQLDataModel() {
      this(null);
   }

   protected NoHQLDataModel(String dataTableId) {
      super();
      this.dataTableId = dataTableId;
   }

   private static boolean areEqualRanges(SequenceRange range1,
                                         SequenceRange range2) {
      if ((range1 == null) || (range2 == null)) {
         return (range1 == null) && (range2 == null);
      } else {
         return (range1.getFirstRow() == range2.getFirstRow())
               && (range1.getRows() == range2.getRows());
      }
   }

   public String getIdPropertyName() {
      return "_id";
   }

   private String getPropertyName(FacesContext facesContext,
                                  Expression expression) {
      try {
         return (String) ((ValueExpression) expression)
               .getValue(facesContext.getELContext());
      } catch (ELException e) {
         throw new FacesException(e.getMessage(), e);
      }
   }

   @Override
   public Object getRowKey() {
      return indexKeys.get(getRowIndex());
   }

   @Override
   public void setRowKey(Object key) {
      if (key == null) {
         setRowIndex(-1);
      } else {
         setRowIndex((Integer) indexKeys.getKey(key));
      }
   }

   public List<T> getAllItems(FacesContext facesContext) {
      walk(facesContext, new DataVisitor() {
         @Override
         public DataVisitResult process(FacesContext facescontext, Object obj, Object obj1) {
            return DataVisitResult.CONTINUE;
         }
      }, null, null);
      return getTypedWrappedData();
   }

   @Override
   public void walk(FacesContext facesContext, DataVisitor visitor,
                    Range range, Object argument) {

      SequenceRange seqRange = (SequenceRange) range;
      if ((getTypedWrappedData() == null) || !areEqualRanges(this.cachedRange, seqRange)) {
         resetCache();
         setCachedRange(seqRange);
         setTypedWrappedData(getQueryBuilder().getList());
      }

      for (T t : getTypedWrappedData()) {
         visitor.process(facesContext, getQueryBuilder().getId(t), argument);
      }
   }

   public abstract NoHQLQueryBuilder<T> newQueryBuilder();

   /**
    * For list compatibility
    *
    * @return row count
    */
   public int size() {
      return getRowCount();
   }

   @Override
   public int getRowCount() {
      if (getTypedWrappedData() == null) {
         setTypedWrappedData(getQueryBuilder().getList());
      }
      return getQueryBuilder().getCount();
   }

   @Override
   public T getRowData() {
      if (getTypedWrappedData() == null) {
         setTypedWrappedData(getQueryBuilder().getList());
      }
      if (isRowAvailable()) {
         return getTypedWrappedData().get(getRowIndex());
      } else {
         return null;
      }
   }

   @Override
   public int getRowIndex() {
      return rowIndex;
   }

   @Override
   public void setRowIndex(int rowIndex) {
      this.rowIndex = rowIndex;
   }

   @Override
   public Object getWrappedData() {
      return weakCache.get();
   }

   public List<T> getTypedWrappedData() {
      return (List<T>) getWrappedData();
   }

   @Override
   public void setWrappedData(Object data) {
         setTypedWrappedData(data != null ? (List<T>) data : null);
   }

   public void setTypedWrappedData(List<T> data) {
      weakCache = new WeakReference<>(data);
      updateKeyIndexes(data);
      rowIndex = -1;
   }

   private void updateKeyIndexes(List<T> data) {
      indexKeys.clear();
      if(data != null) {
         for (int i = 0; i < data.size(); i++) {
            indexKeys.put(i, getQueryBuilder().getId(data.get(i)));
         }
      }
   }

   @Override
   public boolean isRowAvailable() {
      return getTypedWrappedData() != null && (-1 < rowIndex && rowIndex < getTypedWrappedData().size());
   }

   @Override
   public void arrange(FacesContext context, ArrangeableState state) {
      arrangeableState = state;
      resetCache();
   }

   public void resetCache() {
      this.weakCache = new WeakReference<List<T>>(null);
      this.queryBuilder = null;
      this.cachedRange = null;
      indexKeys.clear();
      rowIndex = -1;
   }

   public ArrangeableState getArrangeableState() {
      return arrangeableState;
   }

   public void setArrangeableState(ArrangeableState arrangeableState) {
      this.arrangeableState = arrangeableState;
   }

   protected void appendFilters(FacesContext context, NoHQLQueryBuilder<T> queryBuilder) {

   }

   protected void appendArrangeableState(FacesContext context, NoHQLQueryBuilder<T> queryBuilder) {
      if (arrangeableState != null && (arrangeableState.getFilterFields() != null)
            && (context != null)) {
         for (FilterField filterField : arrangeableState.getFilterFields()) {
            String filterValue = (String) filterField.getFilterValue();
            if ((filterValue != null) && (filterValue.length() != 0)) {
               String propertyName = getPropertyName(context,
                     filterField.getFilterExpression());
               if (propertyName != null) {
                  queryBuilder.addLike(propertyName, filterValue);
               } else {
                  LOGGER.error("{} is not a valid value", filterField.getFilterExpression());
               }
            }
         }
      }
   }

   protected void appendSorts(FacesContext context, NoHQLQueryBuilder<T> queryBuilder) {
      if (arrangeableState != null && (arrangeableState.getSortFields() != null) && (context != null)) {
         for (SortField sortField : arrangeableState.getSortFields()) {
            SortOrder ordering = sortField.getSortOrder();

            if (SortOrder.ascending.equals(ordering)
                  || SortOrder.descending.equals(ordering)) {
               String propertyName = getPropertyName(context,
                     sortField.getSortBy());
               if (propertyName != null) {
                  queryBuilder.addOrder(propertyName,
                        SortOrder.ascending.equals(ordering));
               } else {
                  LOGGER.error("{} is not a valid value", sortField.getSortBy());
               }
            }
         }
      }
   }

   protected void appendPagination(FacesContext facesContext, NoHQLQueryBuilder<T> queryBuilder) {
      queryBuilder.setFirstResult(getCachedRange().getFirstRow());
      queryBuilder.setMaxResults(getCachedRange().getRows());
   }

   protected void prepareQueryBuilder(NoHQLQueryBuilder<T> queryBuilder, FacesContext facesContext) {
      appendFilters(facesContext, queryBuilder);
      appendArrangeableState(facesContext, queryBuilder);
      appendSorts(facesContext, queryBuilder);
      appendPagination(facesContext, queryBuilder);
   }

   private NoHQLQueryBuilder<T> getQueryBuilder() {
      if (queryBuilder == null) {
         queryBuilder = newQueryBuilder();
         prepareQueryBuilder(queryBuilder, FacesContext.getCurrentInstance());
      }
      return queryBuilder;
   }

   private SequenceRange getCachedRange() {
      if (cachedRange == null) {
         cachedRange = new SequenceRange(getRowOffset(), getRows());
      }
      return cachedRange;
   }

   private void setCachedRange(SequenceRange range) {
      this.cachedRange = range;
   }

   private int getRowOffset() {
      if(dataTableId != null) {
         return (getDataScrollerMemory().getScrollerPage().get(dataTableId) - 1) * getRows();
      } else {
         return 0;
      }
   }

   private int getRows() {
      return getDataScrollerMemory().getNumberOfResultsPerPage();
   }

   private static DataScrollerMemory getDataScrollerMemory() {
      return (DataScrollerMemory) Component.getInstance("dataScrollerMemory");
   }

}
