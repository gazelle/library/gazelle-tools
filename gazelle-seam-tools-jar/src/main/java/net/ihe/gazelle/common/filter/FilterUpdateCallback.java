package net.ihe.gazelle.common.filter;

public interface FilterUpdateCallback {

    void filterModified();
}
