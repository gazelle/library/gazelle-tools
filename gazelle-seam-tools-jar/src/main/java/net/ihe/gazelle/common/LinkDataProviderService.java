package net.ihe.gazelle.common;

import net.ihe.gazelle.services.GenericServiceLoader;

import java.util.List;

public class LinkDataProviderService {

    private LinkDataProviderService() {
        super();
    }

    /**
     * @param valueClass class to be supported
     *
     * @return a provider supporting this valueClass
     */
    public static LinkDataProvider getProviderForClass(Class<?> valueClass) {
        List<LinkDataProvider> providers = GenericServiceLoader.getServices(LinkDataProvider.class);
        for (LinkDataProvider dataProvider : providers) {
            List<Class<?>> supportedClasses = dataProvider.getSupportedClasses();
            for (Class<?> supportedClass : supportedClasses) {
                if (supportedClass.isAssignableFrom(valueClass)) {
                    return dataProvider;
                }
            }
        }
        return null;
    }
}
