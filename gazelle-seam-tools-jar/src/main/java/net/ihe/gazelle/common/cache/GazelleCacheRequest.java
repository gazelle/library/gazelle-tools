package net.ihe.gazelle.common.cache;

public interface GazelleCacheRequest<T> {
    T get(String objectId);
}
