package net.ihe.gazelle.common.servletfilter;

import java.util.Map;

public interface CSPPoliciesPreferences {

	boolean isContentPolicyActivated();

	Map<String, String> getHttpSecurityPolicies();

	boolean getSqlInjectionFilterSwitch();
}
