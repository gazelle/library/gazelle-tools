package net.ihe.gazelle.common.pages.menu;

import java.util.List;

import net.ihe.gazelle.common.pages.Page;

public interface Menu {

	Page getPage();

	List<Menu> getItems();

	boolean isVisible();

}
