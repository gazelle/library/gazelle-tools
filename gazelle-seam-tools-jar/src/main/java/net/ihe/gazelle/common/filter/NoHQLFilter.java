package net.ihe.gazelle.common.filter;

import net.ihe.gazelle.common.filter.list.NoHQLDataModel;

import javax.faces.model.SelectItem;
import java.util.List;
import java.util.Map;

public interface NoHQLFilter<T> {

    NoHQLDataModel<T> getDataModel();

    void clear();

    Map<String, String> getFilterValues();

    List<SelectItem>  getPossibleValues(String filterId);

    void setDisplay(String filterId);

    boolean isDisplay(String filterId);

    String format(String selectedValue);

    void resetField(String fieldId);

    String getUrlParameters();

    void setUrlParameters(String urlParameters);

    void appendFilters(NoHQLQueryBuilder<T> queryBuilder, String excludeCriterion);

    void appendFilters(NoHQLQueryBuilder<T> queryBuilder);

    void setDataModel(NoHQLDataModel<T> dataModel);
}
