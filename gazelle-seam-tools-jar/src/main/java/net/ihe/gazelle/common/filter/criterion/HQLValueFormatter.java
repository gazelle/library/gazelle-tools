/*******************************************************************************
 * Copyright 2011 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package net.ihe.gazelle.common.filter.criterion;

import net.ihe.gazelle.common.filter.Filter;
import net.ihe.gazelle.hql.criterion.HQLCriterion;
import org.apache.commons.lang.StringUtils;

import java.io.Serializable;
import java.util.List;

@SuppressWarnings({ "rawtypes", "unchecked" })
public class HQLValueFormatter implements ValueFormatter, Serializable {

	private static final long serialVersionUID = -1377299810393122368L;

	public static final int ABBREVIATION_LENGTH = 50;

	private Filter filter;

	private String keyword;

	public HQLValueFormatter(Filter filter, String keyword) {
		super();
		this.filter = filter;
		this.keyword = keyword;
	}

	// Need to be public because it is called in front in gazelle-TM xhtml
	@Override
	public String format(Object o) {
		String selectableLabel = "?";
		if (o != null) {
			HQLCriterion criterion = (HQLCriterion) filter.getCriterions().get(keyword);
			selectableLabel = Filter.getSelectableLabel(criterion, o);

			selectableLabel = abbreviate(selectableLabel);

			// Add count
			List<Integer> counts = filter.getPossibleValuesCount(keyword);
			if (counts != null) {
				List<Object> values = filter.getPossibleValues(keyword);
				if (values != null) {
					int index = values.indexOf(o);
					if (index != -1) {
						Integer count = counts.get(index);
						if (count >= 0) {
							selectableLabel = "(" + count + ") " + selectableLabel;
						}
					}
				}
			}
		}
		return selectableLabel;
	}

	private String abbreviate(String toAbbreviate) {
		toAbbreviate = StringUtils.abbreviate(toAbbreviate, ABBREVIATION_LENGTH);
		return toAbbreviate;
	}

	@Override
	public String formatLabel(Object o) {
		String selectableLabel = "?";
		if (o != null) {
			HQLCriterion criterion = (HQLCriterion) filter.getCriterions().get(keyword);
			selectableLabel = Filter.getSelectableLabel(criterion, o);
			selectableLabel = abbreviate(selectableLabel);
		}
		return selectableLabel;
	}

	@Override
	public String formatCount(Object o) {
		if (o != null) {
			List<Integer> counts = filter.getPossibleValuesCount(keyword);
			if (counts != null) {
				List<Object> values = filter.getPossibleValues(keyword);
				if (values != null) {
					int index = values.indexOf(o);
					if (index != -1) {
						Integer count = counts.get(index);
						if (count >= 0) {
							return Integer.toString(count);
						}
					}
				}
			}
		}
		return "";
	}

}
