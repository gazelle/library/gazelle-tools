package net.ihe.gazelle.common.excel;

import org.jsoup.helper.StringUtil;
import org.jsoup.nodes.Node;
import org.jsoup.nodes.TextNode;
import org.jsoup.select.NodeVisitor;

public class GazelleNodeVisitor implements NodeVisitor {

    private StringBuilder accum = new StringBuilder();

    private static final int maxWidth = Integer.MAX_VALUE;
    private int width = 0;

    private boolean skipNextText = false;

    @Override
    public void head(Node node, int depth) {
        String name = node.nodeName();
        if (name.equals("option")) {
            String selected = node.attr("selected");
            if (!"selected".equals(selected)) {
                skipNextText = true;
            }
        }
        if (node instanceof TextNode) {
            if (skipNextText) {
                skipNextText = false;
            } else {
                append(((TextNode) node).text());
            }
        } else if (name.equals("li")) {
            append("\n * ");
        }
    }

    @Override
    public void tail(Node node, int depth) {
        String name = node.nodeName();
        if (name.equals("br")) {
            append("\n");
        } else if (StringUtil.in(name, "p", "h1", "h2", "h3", "h4", "h5")) {
            append("\n\n");
        }
    }

    // appends text to the string builder with a simple word wrap method
    private void append(String text) {
        if (text.startsWith("\n")) {
            width = 0;
            // reset counter if starts with a newline. only from
        }
        // formats above, not in natural text
        if (text.equals(" ")
                && ((accum.length() == 0) || StringUtil.in(accum.substring(accum.length() - 1), " ", "\n"))) {
            return;
            // don't accumulate long runs of empty spaces
        }

        if ((text.length() + width) > maxWidth) {
            // won't fit, needs to wrap
            String words[] = text.split("\\s+");
            for (int i = 0; i < words.length; i++) {
                String word = words[i];
                boolean last = i == (words.length - 1);
                if (!last) {
                    word = word + " ";
                }
                if ((word.length() + width) > maxWidth) {
                    // wrap and reset
                    // counter
                    accum.append('\n').append(word);
                    width = word.length();
                } else {
                    accum.append(word);
                    width += word.length();
                }
            }
        } else {
            // fits as is, without need to wrap text
            accum.append(text);
            width += text.length();
        }
    }

    @Override
    public String toString() {
        return accum.toString();
    }
}
