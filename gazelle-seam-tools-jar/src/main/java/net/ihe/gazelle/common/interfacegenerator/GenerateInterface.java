package net.ihe.gazelle.common.interfacegenerator;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

@Target(TYPE)
@Retention(RUNTIME)
public @interface GenerateInterface {

	String value();

	boolean isLocal() default true;

	boolean isRemote() default false;

	String extendsInterface() default "";

}
