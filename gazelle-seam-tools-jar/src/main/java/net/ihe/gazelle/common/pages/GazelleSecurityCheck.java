package net.ihe.gazelle.common.pages;

import net.ihe.gazelle.services.GenericServiceLoader;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.intercept.BypassInterceptors;
import org.jboss.seam.security.Identity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

@Name("gazelleSecurityCheck")
@AutoCreate
@Scope(ScopeType.APPLICATION)
@BypassInterceptors
public class GazelleSecurityCheck implements GazelleSecurityCheckLocal {

    private static Logger log = LoggerFactory.getLogger(GazelleSecurityCheck.class);

    public static ThreadLocal<String> LAST_SERVLET_PATH = new ThreadLocal<String>();

    private static Page[] PAGES = null;

    public static Page[] getPages() {
        if (PAGES == null) {
            synchronized (GazelleSecurityCheck.class) {
                if (PAGES == null) {
                    List<Page> allPages = new ArrayList<Page>();
                    List<PageLister> pageListers = GenericServiceLoader.getServices(PageLister.class);
                    for (PageLister pageLister : pageListers) {
                        allPages.addAll(pageLister.getPages());
                    }
                    PAGES = allPages.toArray(new Page[allPages.size()]);
                }
            }
        }
        return PAGES;
    }

    @Override
    public boolean checkSecurity() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        ExternalContext externalContext = facesContext.getExternalContext();
        externalContext.getResponse();
        HttpServletRequest request = (HttpServletRequest) facesContext.getExternalContext().getRequest();
        String servletPath = request.getServletPath();
        storeRequestURL(request);
        return checkPermission(servletPath);
    }

    public void storeRequestURL(HttpServletRequest request) {
        String requestURL = getServletURL(request);
        LAST_SERVLET_PATH.set(requestURL);
    }

    public static String getServletURL(HttpServletRequest request) {
        StringBuffer url = request.getRequestURL();
        String queryString = request.getQueryString();
        if (queryString != null) {
            url.append('?');
            url.append(queryString);
        }
        return url.toString();
    }

    protected boolean checkPermission(String servletPath) {

        String servletPathXHTML = servletPath.replace(".seam", ".xhtml");
        String servletPathSEAM = servletPath.replace(".xhtml", ".seam");

        boolean checked = false;
        boolean authorized = false;
        if (servletPathXHTML.startsWith("/a4j/")) {
            checked = true;
            authorized = true;
        } else {
            Page[] pages = getPages();
            for (Page page : pages) {
                if (page.getLink().startsWith(servletPathXHTML) || page.getLink().startsWith(servletPathSEAM)) {
                    checked = true;
                    if (PageCheckAuthorization.isGranted(page)) {
                        authorized = true;
                    }
                }
            }
        }
        if (checked) {
            if (!authorized) {
                String username = Identity.instance().getCredentials().getUsername();
                if (username == null) {
                    username = "Not logged in user";
                }
                log.warn(" Access denied. " + Identity.instance().getCredentials().getUsername()
                        + " has insuffient permission to access : " + servletPath);
                return false;
            }
        } else {
            log.error("No access rule defined for " + servletPath);
            // FIXME strict security mode
            // return false;
        }

        return true;
    }
}
