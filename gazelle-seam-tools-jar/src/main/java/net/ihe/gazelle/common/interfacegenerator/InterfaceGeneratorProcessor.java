package net.ihe.gazelle.common.interfacegenerator;

import org.apache.commons.io.IOUtils;

import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.FilerException;
import javax.annotation.processing.RoundEnvironment;
import javax.annotation.processing.SupportedAnnotationTypes;
import javax.annotation.processing.SupportedSourceVersion;
import javax.lang.model.element.Element;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.Name;
import javax.lang.model.element.PackageElement;
import javax.lang.model.element.TypeElement;
import javax.lang.model.element.VariableElement;
import javax.lang.model.type.DeclaredType;
import javax.lang.model.type.TypeMirror;
import javax.tools.FileObject;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import static javax.lang.model.SourceVersion.RELEASE_7;

@SupportedAnnotationTypes({"net.ihe.gazelle.common.interfacegenerator.GenerateInterface"})
@SupportedSourceVersion(RELEASE_7)
public class InterfaceGeneratorProcessor extends AbstractProcessor {

    @Override
    public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnvironment) {
        if (roundEnvironment.processingOver() || (annotations.size() == 0)) {
            return false;
        }

        System.out.println(InterfaceGeneratorProcessor.class.getCanonicalName() + " processor");
        System.out.println("Generating interfaces thanks to @GenerateInterface");
        Set<? extends Element> elements = roundEnvironment.getRootElements();
        for (Element element : elements) {
            if (element.getAnnotation(GenerateInterface.class) != null) {
                generateInterface(element, element.getAnnotation(GenerateInterface.class));
            }
        }

        return false;
    }

    private void generateInterface(Element element, GenerateInterface generateInterface) {
        OutputStream outputStream = null;
        try {
            String interfaceName = generateInterface.value();
            String interfacePackage = getPackageName(element);

            Map<String, String> methods = new TreeMap<String, String>();

            getPublicMethods(element, methods);

            String className = interfacePackage + "." + interfaceName;
            FileObject fo = processingEnv.getFiler().createSourceFile(className);
            outputStream = fo.openOutputStream();
            PrintWriter pw = new PrintWriter(outputStream);

            pw.println("package " + interfacePackage + ";");
            pw.println();
            if (generateInterface.isLocal()) {
                pw.println("@javax.ejb.Local");
            }
            if (generateInterface.isRemote()) {
                pw.println("@javax.ejb.Remote");
            }
            if ("".equals(generateInterface.extendsInterface())) {
                pw.println("public interface " + interfaceName + " {");
            } else {
                pw.println("public interface " + interfaceName + " extends " + generateInterface.extendsInterface()
                        + " {");
            }
            for (String methodSignature : methods.values()) {
                pw.println("");
                pw.println(methodSignature);
            }
            pw.println("");
            pw.println('}');

            pw.flush();
            pw.close();

            //System.out.println("Generated " + className);
        } catch (FilerException filerEx) {
            filerEx.printStackTrace(System.err);
        } catch (IOException ioEx) {
            ioEx.printStackTrace(System.err);
        } finally {
            IOUtils.closeQuietly(outputStream);
        }
    }

    private void getPublicMethods(Element element, Map<String, String> methods) {
        List<? extends Element> enclosedElements = element.getEnclosedElements();
        for (Element enclosedElement : enclosedElements) {
            if (enclosedElement instanceof ExecutableElement) {
                ExecutableElement executableElement = (ExecutableElement) enclosedElement;
                Set<javax.lang.model.element.Modifier> modifiers = executableElement.getModifiers();
                if (modifiers.contains(javax.lang.model.element.Modifier.PUBLIC)
                        && !modifiers.contains(javax.lang.model.element.Modifier.STATIC)) {
                    String simpleName = executableElement.getSimpleName().toString();
                    // do not add constructor
                    if (!simpleName.equals("<init>")) {
                        methods.put(getSimpleSignature(executableElement), getSignature(executableElement));
                    }
                }
            }
        }
        if (element instanceof TypeElement) {
            TypeElement typeElement = (TypeElement) element;
            TypeMirror superclass = typeElement.getSuperclass();
            if ((superclass != null) && (superclass instanceof DeclaredType)) {
                DeclaredType superElement = (DeclaredType) superclass;
                Element asElement = superElement.asElement();
                boolean process = true;
                if (asElement instanceof TypeElement) {
                    TypeElement parentElement = (TypeElement) asElement;
                    String parentQualifiedName = parentElement.getQualifiedName().toString();
                    if (parentQualifiedName.equals("java.lang.Object")) {
                        process = false;
                    }
                    if (process) {
                        getPublicMethods(asElement, methods);
                    }
                }
            }
        }
    }

    private String getSimpleSignature(ExecutableElement method) {
        StringBuilder sb = new StringBuilder("");

        Name simpleName = method.getSimpleName();
        sb.append(simpleName);
        sb.append('(');

        List<? extends VariableElement> parameters = method.getParameters();
        boolean first = true;
        for (VariableElement parameter : parameters) {
            if (first) {
                first = false;
            } else {
                sb.append(", ");
            }

            sb.append(parameter.asType().toString());
        }

        sb.append(')');

        List<? extends TypeMirror> thrownTypes = method.getThrownTypes();
        if (thrownTypes.size() > 0) {
            sb.append(" throws ");
            first = true;
            for (TypeMirror typeMirror : thrownTypes) {
                if (first) {
                    first = false;
                } else {
                    sb.append(", ");
                }
                sb.append(typeMirror.toString());
            }
        }
        sb.append(' ');
        sb.append(method.getReturnType().toString());

        return sb.toString();
    }

    private String getSignature(ExecutableElement method) {
        StringBuilder sb = new StringBuilder("	");
        sb.append(method.getReturnType().toString());
        sb.append(' ');

        Name simpleName = method.getSimpleName();
        sb.append(simpleName);
        sb.append('(');

        List<? extends VariableElement> parameters = method.getParameters();
        boolean first = true;
        for (VariableElement parameter : parameters) {
            if (first) {
                first = false;
            } else {
                sb.append(", ");
            }

            sb.append(parameter.asType().toString());
            sb.append(' ');
            sb.append(parameter.getSimpleName());
        }

        sb.append(')');

        List<? extends TypeMirror> thrownTypes = method.getThrownTypes();
        if (thrownTypes.size() > 0) {
            sb.append(" throws ");
            first = true;
            for (TypeMirror typeMirror : thrownTypes) {
                if (first) {
                    first = false;
                } else {
                    sb.append(", ");
                }
                sb.append(typeMirror.toString());
            }
        }
        sb.append(';');

        return sb.toString();
    }

    private String getPackageName(Element element) {
        PackageElement packageElement = (PackageElement) element.getEnclosingElement();
        return packageElement.getQualifiedName().toString();
    }
}
