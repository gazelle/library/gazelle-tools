package net.ihe.gazelle.common.jsf;

import javax.faces.model.SelectItem;

/**
 * Convert objects to {@link SelectItem} for JSF components
 *
 * @author ceoche
 */
public class SelectItemConverter {

    /**
     * Convert a collection of objects as {@link SelectItem} for JSF components such as h:SelectMenu.
     * The objects must implement {@link Labelable}.
     *
     * @return the given collection converted as {@link SelectItem} array.
     */
    public static SelectItem[] asSelectItems(Labelable[] items) {
        SelectItem[] selectItems = new SelectItem[items.length];
        for (int i = 0; i < items.length; i++) {
            selectItems[i] = new SelectItem(items[i], items[i].getDisplayLabel());
        }
        return selectItems;
    }

}
