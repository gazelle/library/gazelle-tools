package net.ihe.gazelle.common.filter;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class MapPossibleValuesWrapper implements Map<String, List<Object>>, Serializable {

	private static final long serialVersionUID = -4792038068493341033L;

	private Filter filter;

	public MapPossibleValuesWrapper(Filter filter) {
		super();
		this.filter = filter;
	}

	@Override
	public int size() {
		return filter.getCriterions().size();
	}

	@Override
	public boolean isEmpty() {
		return filter.getCriterions().isEmpty();
	}

	@Override
	public boolean containsKey(Object key) {
		return filter.getCriterions().containsKey(key);
	}

	@Override
	public boolean containsValue(Object value) {
		throw new UnsupportedOperationException();
	}

	@Override
	public List<Object> get(Object key) {
		return filter.getPossibleValues((String) key);
	}

	@Override
	public List<Object> put(String key, List<Object> value) {
		throw new UnsupportedOperationException();
	}

	@Override
	public List<Object> remove(Object key) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void putAll(Map<? extends String, ? extends List<Object>> m) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void clear() {
		throw new UnsupportedOperationException();
	}

	@Override
	public Set<String> keySet() {
		return filter.getCriterions().keySet();
	}

	@Override
	public Collection<List<Object>> values() {
		throw new UnsupportedOperationException();
	}

	@Override
	public Set<java.util.Map.Entry<String, List<Object>>> entrySet() {
		throw new UnsupportedOperationException();
	}

}
