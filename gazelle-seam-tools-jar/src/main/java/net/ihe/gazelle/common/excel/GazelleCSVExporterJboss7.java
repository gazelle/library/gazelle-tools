package net.ihe.gazelle.common.excel;

import net.ihe.gazelle.common.filter.HibernateDataModel;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Install;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.intercept.BypassInterceptors;
import org.jboss.seam.core.Interpolator;
import org.jboss.seam.core.Manager;
import org.jboss.seam.document.ByteArrayDocumentData;
import org.jboss.seam.document.DocumentStore;
import org.jboss.seam.excel.ExcelFactory;
import org.jboss.seam.excel.ExcelWorkbook;
import org.jboss.seam.excel.ExcelWorkbookException;
import org.jboss.seam.excel.css.CSSParser;
import org.jboss.seam.excel.css.ColumnStyle;
import org.jboss.seam.excel.css.StyleMap;
import org.jboss.seam.excel.ui.ExcelComponent;
import org.jboss.seam.excel.ui.UICell;
import org.jboss.seam.excel.ui.UIWorkbook;
import org.jboss.seam.excel.ui.UIWorksheet;
import org.jboss.seam.framework.Query;
import org.jboss.seam.navigation.Pages;
import org.richfaces.component.UIDataTable;

import javax.faces.component.UIColumn;
import javax.faces.component.UIComponent;
import javax.faces.component.UIOutput;
import javax.faces.context.FacesContext;
import javax.faces.model.DataModel;
import java.io.IOException;
import java.lang.reflect.Array;
import java.nio.charset.StandardCharsets;
import java.util.*;

/**
 * Created by gthomazon on 08/12/15.
 */
@Name("gazelleCSVExporterJboss7")
@Scope(ScopeType.EVENT)
@Install(precedence = Install.BUILT_IN)
@BypassInterceptors
public class GazelleCSVExporterJboss7 {

    private ExcelWorkbook excelWorkbook = null;
    private Map<Integer, Integer> columnWidths = new HashMap();

    public void exportCSV(String dataTableId) {
        export(dataTableId, "csv");
    }

    public void export(String dataTableId, String type) {
        this.excelWorkbook = ExcelFactory.instance().getExcelWorkbook(type);
        CSSParser parser = new CSSParser();
        UIDataTable dataTable = (UIDataTable) FacesContext.getCurrentInstance().getViewRoot()
                .findComponent(dataTableId);
        if (dataTable == null) {
            throw new ExcelWorkbookException(Interpolator.instance().interpolate("Could not find data table with id #0", new Object[]{dataTableId}));
        } else {
            UIWorkbook uiWorkbook = new UIWorkbook();
            this.excelWorkbook.createWorkbook(uiWorkbook);
            UIWorksheet uiWorksheet = new UIWorksheet();
            uiWorkbook.getChildren().add(uiWorksheet);
            uiWorksheet.setStyle(CSSParser.getStyle(dataTable));
            uiWorksheet.setStyleClass(CSSParser.getStyleClass(dataTable));
            this.excelWorkbook.createOrSelectWorksheet(uiWorksheet);
            String dataTableVar = dataTable.getVar();
            Object oldValue = FacesContext.getCurrentInstance().getExternalContext().getRequestMap().get(dataTableVar);
            List columns = ExcelComponent.getChildrenOfType(dataTable.getChildren(), UIColumn.class);
            this.columnWidths = this.parseColumnWidths(uiWorksheet);
            int col = 0;
            Iterator iter = columns.iterator();

            while (iter.hasNext()) {
                UIColumn column = (UIColumn) iter.next();
                ColumnStyle columnStyle = new ColumnStyle(parser.getCascadedStyleMap(column));
                boolean cssExport = columnStyle.export == null || columnStyle.export.booleanValue();
                if (column.isRendered() && cssExport) {
                    uiWorksheet.getChildren().add(column);
                    Iterator iterator = unwrapIterator(dataTable.getValue());
                    this.processColumn(column, iterator, dataTableVar, col++);
                    this.excelWorkbook.nextColumn();
                }
            }

            if (oldValue == null) {
                FacesContext.getCurrentInstance().getExternalContext().getRequestMap().remove(dataTableVar);
            } else {
                FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put(dataTableVar, oldValue);
            }

            this.redirectExport();
        }
    }

    private Map<Integer, Integer> parseColumnWidths(UIWorksheet worksheet) {
        HashMap columnWidths = new HashMap();
        CSSParser parser = new CSSParser();
        StyleMap styleMap = parser.getCascadedStyleMap(worksheet);
        Iterator iter = styleMap.entrySet().iterator();

        while (iter.hasNext()) {
            Map.Entry entry = (Map.Entry) iter.next();
            String key = (String) entry.getKey();
            if (key.startsWith("xls-column-widths")) {
                String columnIndexString = key.substring("xls-column-widths".length());
                int columnIndex = Integer.valueOf(columnIndexString);
                columnWidths.put(Integer.valueOf(columnIndex), (Integer) entry.getValue());
            }
        }

        return columnWidths;
    }

    private void redirectExport() {
        String viewId = Pages.getViewId(FacesContext.getCurrentInstance());
        String baseName = Pages.getCurrentBaseName();
        ByteArrayDocumentData documentData = new ByteArrayDocumentData(baseName, this.excelWorkbook.getDocumentType(), this.excelWorkbook.getBytes());
        String id = DocumentStore.instance().newId();
        String url = DocumentStore.instance().preferredUrlForContent(baseName, this.excelWorkbook.getDocumentType().getExtension(), id);
        url = Manager.instance().encodeConversationId(url, viewId);
        DocumentStore.instance().saveData(id, documentData);

        try {
            FacesContext.getCurrentInstance().getExternalContext().redirect(url);
        } catch (IOException var7) {
            throw new ExcelWorkbookException(Interpolator.instance().interpolate("Could not redirect to #0", new Object[]{url}), var7);
        }
    }

    private void processColumn(UIColumn column, Iterator iterator, String var, int columnIndex) {
        UIComponent headerFacet = column.getFacet("header");
        if (headerFacet != null && UIOutput.class.isAssignableFrom(headerFacet.getClass())) {
            ArrayList columnWidth = new ArrayList();
            columnWidth.add((UIOutput) headerFacet);
            this.processOutputs(column, columnWidth);
        }

        while (iterator.hasNext()) {
            FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put(var, iterator.next());
            List columnWidth2 = ExcelComponent.getChildrenOfType(column.getChildren(), UIOutput.class);
            this.processOutputs(column, columnWidth2);
        }

        Integer columnWidth1 = (Integer) this.columnWidths.get(Integer.valueOf(columnIndex));
        if (columnWidth1 != null) {
            org.jboss.seam.excel.ui.UIColumn uiColumn = new org.jboss.seam.excel.ui.UIColumn();
            uiColumn.setStyle("xls-column-width:" + columnWidth1);
            this.excelWorkbook.applyColumnSettings(uiColumn);
        }
    }

    private void processOutputs(UIColumn column, List<UIOutput> outputs) {
        Iterator iter = outputs.iterator();

        while (iter.hasNext()) {
            UIOutput output = (UIOutput) iter.next();
            if (output.isRendered()) {
                UICell cell = new UICell();
                column.getChildren().add(cell);
                cell.setId(output.getId());
                cell.setValue(output.getValue());
                cell.setStyle(CSSParser.getStyle(output));
                cell.setStyleClass(CSSParser.getStyleClass(output));
                this.excelWorkbook.addItem(cell);
            }
        }
    }

    private static Iterator unwrapIterator(Object value) {
        if (value instanceof HibernateDataModel) {
            return ((HibernateDataModel) value).getAllItems(FacesContext.getCurrentInstance()).iterator();
        } else if (value instanceof Iterable) {
            return ((Iterable) value).iterator();
        } else if ((value instanceof DataModel) && (((DataModel) value).getWrappedData() instanceof Iterable)) {
            return ((Iterable) ((DataModel) value).getWrappedData()).iterator();
        } else if (value instanceof Map) {
            return ((Map) value).entrySet().iterator();
        } else if (value instanceof Query) {
            return (((Query) value).getResultList()).iterator();
        } else if ((value != null) && value.getClass().isArray()) {
            return arrayAsList(value).iterator();
        } else {
            throw new RuntimeException("A worksheet's value must be an Iterable, DataModel or Query");
        }
    }

    private static List arrayAsList(Object array) {
        if (array.getClass().getComponentType().isPrimitive()) {
            List list = new ArrayList();
            for (int i = 0; i < Array.getLength(array); i++) {
                list.add(Array.get(array, i));
            }
            return list;
        } else {
            return Arrays.asList((Object[]) array);
        }
    }
}
