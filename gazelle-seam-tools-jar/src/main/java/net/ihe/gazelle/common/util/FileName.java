/*
 * Copyright 2009 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.ihe.gazelle.common.util;

/**
 * @author Abderrazek Boufahja / INRIA Rennes IHE development Project
 */
public class FileName {

    private static char extensionSeparator = '.';
    private static char pathSeparator = '/';

    public FileName() {
        extensionSeparator = '.';
        pathSeparator = '/';
    }

    public FileName(char sep, char ext) {
        pathSeparator = sep;
        extensionSeparator = ext;
    }

    public static String extension(String fullPath) {
        String result = "";
        if (fullPath != null) {
            int dot = fullPath.lastIndexOf(extensionSeparator);
            result = fullPath.substring(dot + 1);
        }
        return result;
    }

    /**
     * gets filename without extension
     */
    public static String getFileNameWithoutExtention(String fullPath) {
        String result = "";
        if (fullPath != null) {
            int dot = fullPath.lastIndexOf(extensionSeparator);
            int sep = fullPath.lastIndexOf(pathSeparator);
            result = fullPath.substring(sep + 1, dot);
        }
        return result;
    }

    /**
     * gets filename with extension
     */
    public String getFileNameWithExtention(String fullPath) {
        String result = "";
        if (fullPath != null) {
            int sep = fullPath.lastIndexOf(pathSeparator);
            result = fullPath.substring(sep + 1, fullPath.length());
        }
        return result;
    }

    public String path(String fullPath) {
        String result = "";
        if (fullPath != null) {
            int sep = fullPath.lastIndexOf(pathSeparator);
            result = fullPath.substring(0, sep);
        }
        return result;
    }

}
