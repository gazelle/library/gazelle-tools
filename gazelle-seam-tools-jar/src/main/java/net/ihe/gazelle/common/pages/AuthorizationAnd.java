package net.ihe.gazelle.common.pages;

public class AuthorizationAnd implements Authorization {

	private Authorization[] authorizations;

	public AuthorizationAnd(Authorization... authorizations) {
		super();
		this.authorizations = authorizations;
	}

	@Override
	public boolean isGranted(Object... context) {
		for (Authorization authorization : authorizations) {
			if (!authorization.isGranted(context)) {
				return false;
			}
		}
		return true;
	}

}
