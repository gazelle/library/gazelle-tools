package net.ihe.gazelle.common.pages;

public interface Authorization {

	boolean isGranted(Object... context);

}
