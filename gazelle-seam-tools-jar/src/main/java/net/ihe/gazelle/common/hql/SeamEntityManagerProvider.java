package net.ihe.gazelle.common.hql;

import net.ihe.gazelle.hql.providers.EntityManagerProvider;
import org.jboss.seam.Component;
import org.jboss.seam.contexts.Contexts;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;

public class SeamEntityManagerProvider implements EntityManagerProvider {

    private static final Logger log = LoggerFactory.getLogger(SeamEntityManagerProvider.class);
    private static final int LIGHT_WEIGHT = -100;
    private static final int HEAVY_WEIGHT = 100;

    @Override
    public EntityManager provideEntityManager() {
        if (Contexts.isApplicationContextActive()) {
            return (EntityManager) Component.getInstance("entityManager");
        } else {
            log.error("SeamEntityManagerProvider used but no Seam context!");
            return null;
        }
    }

    @Override
    public Integer getWeight() {
        if (Contexts.isApplicationContextActive()) {
            return LIGHT_WEIGHT;
        } else {
            return HEAVY_WEIGHT;
        }
    }

    @Override
    public int compareTo(EntityManagerProvider o) {
        return getWeight().compareTo(o.getWeight());
    }
}
