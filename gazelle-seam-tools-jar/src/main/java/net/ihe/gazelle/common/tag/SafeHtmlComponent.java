package net.ihe.gazelle.common.tag;

import com.google.common.base.Predicate;
import org.owasp.html.HtmlPolicyBuilder;
import org.owasp.html.PolicyFactory;

import javax.faces.component.UIComponentBase;
import javax.faces.context.FacesContext;
import javax.faces.context.ResponseWriter;
import java.io.IOException;
import java.util.regex.Pattern;

public class SafeHtmlComponent extends UIComponentBase {

    // The 16 colors defined by the HTML Spec (also used by the CSS Spec)
    private static final Pattern COLOR_NAME = Pattern.compile(
            "(?:aqua|black|blue|fuchsia|gray|grey|green|lime|maroon|navy|olive|purple"
                    + "|red|silver|teal|white|yellow)");

    // HTML/CSS Spec allows 3 or 6 digit hex to specify color
    private static final Pattern COLOR_CODE = Pattern.compile(
            "(?:#(?:[0-9a-fA-F]{3}(?:[0-9a-fA-F]{3})?))");

    private static final Pattern NUMBER_OR_PERCENT = Pattern.compile(
            "[0-9]+%?");
    private static final Pattern PARAGRAPH = Pattern.compile(
            "(?:[\\p{L}\\p{N},'\\.\\s\\-_\\(\\)]|&[0-9]{2};)*");
    private static final Pattern HTML_ID = Pattern.compile(
            "[a-zA-Z0-9\\:\\-_\\.]+");
    // force non-empty with a '+' at the end instead of '*'
    private static final Pattern HTML_TITLE = Pattern.compile(
            "[\\p{L}\\p{N}\\s\\-_',:\\[\\]!\\./\\\\\\(\\)&]*");
    private static final Pattern HTML_CLASS = Pattern.compile(
            "[a-zA-Z0-9\\s,\\-_]+");

    private static final Pattern ONSITE_URL = Pattern.compile(
            "(?:[\\p{L}\\p{N}\\\\\\.\\#@\\$%\\+&;\\-_~,\\?=/!]+|\\#(\\w)+)");

    private static final Pattern OFFSITE_URL = Pattern.compile(
            "\\s*(?:(?:ht|f)tps?://|mailto:)[\\p{L}\\p{N}]"
                    + "[\\p{L}\\p{N}\\p{Zs}\\.\\#@\\$%\\+&;:\\-_~,\\?=/!\\(\\)]*+\\s*");

    private static final Pattern HTML_B64_IMG = Pattern.compile("data:image\\/([a-zA-Z]*);base64,([^\\\"]*)");

    private static final Pattern NUMBER = Pattern.compile(
            "[+-]?(?:(?:[0-9]+(?:\\.[0-9]*)?)|\\.[0-9]+)");

    private static final Pattern NAME = Pattern.compile("[a-zA-Z0-9\\-_\\$]+");
    private static final Pattern FILENAME = Pattern.compile("[a-zA-Z0-9\\-_\\$]+\\.[A-Za-z]+");

    private static final Pattern ALIGN = Pattern.compile(
            "(?i)center|left|right|justify|char");

    private static final Pattern VALIGN = Pattern.compile(
            "(?i)baseline|bottom|middle|top");

    private static final Predicate<String> COLOR_NAME_OR_COLOR_CODE
            = matchesEither(COLOR_NAME, COLOR_CODE);

    private static final Predicate<String> ONSITE_OR_OFFSITE_URL
            = matchesEither(ONSITE_URL, OFFSITE_URL);

    private static final Predicate<String> SRC_IMG
            = matchesEither(ONSITE_URL, OFFSITE_URL, HTML_B64_IMG);

    private static final Pattern HISTORY_BACK = Pattern.compile(
            "(?:javascript:)?\\Qhistory.go(-1)\\E");

    private static final Pattern ONE_CHAR = Pattern.compile(
            ".?", Pattern.DOTALL);

    private static Predicate<String> matchesEither(
            final Pattern a, final Pattern b) {
        return new Predicate<String>() {
            public boolean apply(String s) {
                return a.matcher(s).matches() || b.matcher(s).matches();
            }
        };
    }

    private static Predicate<String> matchesEither(
            final Pattern a, final Pattern b, final Pattern c) {
        return new Predicate<String>() {
            public boolean apply(String s) {
                return a.matcher(s).matches() || b.matcher(s).matches() || c.matcher(s).matches();
            }
        };
    }

    public static final String FONT = "font";
    public static final String IMG = "img";
    public static final String TABLE = "table";
    public static final String THEAD = "thead";
    public static final String TBODY = "tbody";
    public static final String TFOOT = "tfoot";
    public static final String COLGROUP = "colgroup";
    public static final String COL = "col";
    /**
     * A policy that can be used to produce policies that sanitize to HTML sinks
     * via {@link PolicyFactory#apply}.
     */
    public static final PolicyFactory POLICY_DEFINITION = new HtmlPolicyBuilder()
            .allowAttributes("id").matching(HTML_ID).globally()
            .allowAttributes("class").matching(HTML_CLASS).globally()
            .allowAttributes("lang").matching(Pattern.compile("[a-zA-Z]{2,20}"))
            .globally()
            .allowAttributes("title").matching(HTML_TITLE).globally()
            .allowStyling()
            .allowAttributes("align").matching(ALIGN).onElements("p")
            .allowAttributes("style").onElements("p")
            .allowAttributes("for").matching(HTML_ID).onElements("label")
            .allowAttributes("color").matching(COLOR_NAME_OR_COLOR_CODE)
            .onElements(FONT)
            .allowAttributes("face")
            .matching(Pattern.compile("[\\w;, \\-]+"))
            .onElements(FONT)
            .allowAttributes("size").matching(NUMBER).onElements(FONT)
            .allowAttributes("href").matching(ONSITE_OR_OFFSITE_URL)
            .onElements("a")
            .allowStandardUrlProtocols()
            .allowAttributes("nohref").onElements("a")
            .allowAttributes("target").onElements("a")
            .allowAttributes("name").matching(NAME).onElements("a")
            .allowAttributes(
                    "onfocus", "onblur", "onclick", "onmousedown", "onmouseup")
            .matching(HISTORY_BACK).onElements("a")
            .requireRelNofollowOnLinks()
            .allowUrlProtocols("data").allowAttributes("src").matching(SRC_IMG)
            .onElements(IMG)
            .allowAttributes("data-filename").matching(FILENAME)
            .onElements(IMG)
            .allowAttributes("name").matching(NAME)
            .onElements(IMG)
            .allowAttributes("alt").matching(PARAGRAPH)
            .onElements(IMG)
            .allowAttributes("border", "hspace", "vspace").matching(NUMBER)
            .onElements(IMG)
            .allowAttributes("border", "cellpadding", "cellspacing")
            .matching(NUMBER).onElements(TABLE)
            .allowAttributes("bgcolor").matching(COLOR_NAME_OR_COLOR_CODE)
            .onElements(TABLE)
            .allowAttributes("background").matching(ONSITE_URL)
            .onElements(TABLE)
            .allowAttributes("align").matching(ALIGN)
            .onElements(TABLE)
            .allowAttributes("noresize").matching(Pattern.compile("(?i)noresize"))
            .onElements(TABLE)
            .allowAttributes("background").matching(ONSITE_URL)
            .onElements("td", "th", "tr")
            .allowAttributes("bgcolor").matching(COLOR_NAME_OR_COLOR_CODE)
            .onElements("td", "th")
            .allowAttributes("abbr").matching(PARAGRAPH)
            .onElements("td", "th")
            .allowAttributes("axis", "headers").matching(NAME)
            .onElements("td", "th")
            .allowAttributes("scope")
            .matching(Pattern.compile("(?i)(?:row|col)(?:group)?"))
            .onElements("td", "th")
            .allowAttributes("nowrap")
            .onElements("td", "th")
            .allowAttributes("height", "width").matching(NUMBER_OR_PERCENT)
            .onElements(TABLE, "td", "th", "tr", IMG)
            .allowAttributes("align").matching(ALIGN)
            .onElements(THEAD, TBODY, TFOOT, IMG,
                    "td", "th", "tr", COLGROUP, COL)
            .allowAttributes("valign").matching(VALIGN)
            .onElements(THEAD, TBODY, TFOOT,
                    "td", "th", "tr", COLGROUP, COL)
            .allowAttributes("charoff").matching(NUMBER_OR_PERCENT)
            .onElements("td", "th", "tr", COLGROUP, COL,
                    THEAD, TBODY, TFOOT)
            .allowAttributes("char").matching(ONE_CHAR)
            .onElements("td", "th", "tr", COLGROUP, COL,
                    THEAD, TBODY, TFOOT)
            .allowAttributes("colspan", "rowspan").matching(NUMBER)
            .onElements("td", "th")
            .allowAttributes("span", "width").matching(NUMBER_OR_PERCENT)
            .onElements(COLGROUP, COL)
            .allowElements(
                    "a", "label", "noscript", "h1", "h2", "h3", "h4", "h5", "h6",
                    "p", "i", "b", "u", "strong", "em", "small", "big", "pre", "code",
                    "cite", "samp", "sub", "sup", "strike", "center", "blockquote",
                    "hr", "br", COL, FONT, "map", "span", "div", IMG,
                    "ul", "ol", "li", "dd", "dt", "dl", TBODY, THEAD, TFOOT,
                    TABLE, "td", "th", "tr", COLGROUP, "fieldset", "legend", "caption")
            .toFactory();

    public static PolicyFactory getPolicy() {
        return POLICY_DEFINITION;
    }

    @Override
    public String getFamily() {
        return "gazelle-safehtml";
    }

    @Override
    public void encodeBegin(FacesContext context) throws IOException {
        ResponseWriter writer = context.getResponseWriter();

        Object value = getAttributes().get("value");

        if (value != null) {
            String sanitized = POLICY_DEFINITION.sanitize(value.toString());
            writer.write(sanitized);
        }

        writer.flush();
    }

    @Override
    public void encodeEnd(FacesContext context) throws IOException {
        if (isRendered()) {
            return;
        }
    }
}
