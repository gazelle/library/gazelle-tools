package net.ihe.gazelle.common.tree;

import org.apache.commons.digester.Digester;
import org.apache.commons.digester.RulesBase;
import org.richfaces.model.TreeNode;
import org.slf4j.LoggerFactory;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class XmlTreeDataBuilder {
    private XmlTreeDataBuilder() {
    }

    private static final class Rule extends org.apache.commons.digester.Rule {
        private int level = -1;
        private List idsList = new ArrayList();
        private List treeNodesList = new ArrayList();
        private List exclusionSets = new ArrayList();
        //add empty node to serve as root
        private GazelleTreeNodeImpl treeNode = new GazelleTreeNodeImpl();

        public void begin(String namespace, String name, Attributes attributes)
                throws Exception {
            super.begin(namespace, name, attributes);

            level++;

            XmlNodeData xmlNodeData = new XmlNodeData();
            xmlNodeData.setName(name);
            xmlNodeData.setNamespace(namespace);

            String id = null;

            if (attributes != null) {
                int length = attributes.getLength();
                for (int i = 0; i < length; i++) {
                    xmlNodeData.setAttribute(attributes.getQName(i),
                            attributes.getValue(i));
                }

                id = attributes.getValue("id");
            }

            if (exclusionSets.size() == level) {
                exclusionSets.add(null);
            }

            if (id == null || id.length() == 0) {
                int currentId = 0;

                if (idsList.size() <= level) {
                    for (int i = idsList.size(); i <= level; i++) {
                        idsList.add(null);
                    }
                } else {
                    Integer integer = (Integer) idsList.get(level);
                    currentId = integer.intValue() + 1;
                }

                Set exclusions = (Set) exclusionSets.get(level);

                while (exclusions != null && exclusions.contains(Integer.toString(currentId))) {
                    currentId++;
                }

                idsList.set(level, new Integer(currentId));

                id = Integer.toString(currentId);
            } else {
                Set exclusions = (Set) exclusionSets.get(level);
                if (exclusions == null) {
                    exclusions = new HashSet();

                    exclusionSets.set(level, exclusions);
                }

                exclusions.add(id);
            }

            GazelleTreeNodeImpl node = new GazelleTreeNodeImpl();
            node.setData(xmlNodeData);

            this.treeNode.addChild(id, node);
            this.treeNodesList.add(this.treeNode);
            this.treeNode = node;
        }

        public void body(String namespace, String name, String text)
                throws Exception {
            super.body(namespace, name, text);

            if (text != null) {
                ((XmlNodeData) this.treeNode.getData()).setText(text.trim());
            }
        }

        public void end(String namespace, String name) throws Exception {
            super.end(namespace, name);

            level--;

            if (idsList.size() - 1 > level + 1) {
                //idsList grew larger than we really need
                idsList.remove(idsList.size() - 1);
            }

            if (exclusionSets.size() - 1 > level + 1) {
                //the same condition as above
                exclusionSets.remove(exclusionSets.size() - 1);
            }

            this.treeNode = (GazelleTreeNodeImpl) this.treeNodesList.remove(this.treeNodesList.size() - 1);
        }
    }

    public static TreeNode build(InputSource inputSource) throws SAXException, IOException {
        Digester digester = new Digester();
        Rule rule = new Rule();
        final List rulesList = new ArrayList(1);
        rulesList.add(rule);

        RulesBase rulesBase = new RulesBase() {
            protected List lookup(String namespace, String name) {
                return rulesList;
            }
        };

        try {
            digester.setFeature("http://apache.org/xml/features/disallow-doctype-decl", true);
            digester.setRules(rulesBase);
            digester.setNamespaceAware(true);
            digester.parse(inputSource);
        } catch (Exception e) {
            LoggerFactory.getLogger("Error performing tree transformation of file");
            return null;
        }

        return rule.treeNode;
    }
}
