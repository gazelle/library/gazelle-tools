package net.ihe.gazelle.common.util;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipOutputStream;

public class ZipUtility {

    public static final int BUFFER_SIZE = 8192;
    private static Logger log = LoggerFactory.getLogger(ZipUtility.class);

    private ZipUtility() {
    }

    public static void zipDirectory(File directory, File zip) throws IOException {
        ZipOutputStream zos = new ZipOutputStream(new FileOutputStream(zip));
        zip(directory, directory, zos);
        zos.close();
    }

    private static void zip(File directory, File base, ZipOutputStream zos) throws IOException {
        File[] files = directory.listFiles();
        byte[] buffer = new byte[BUFFER_SIZE];
        int read = 0;
        for (int i = 0, n = files.length; i < n; i++) {
            if (files[i].isDirectory()) {
                zip(files[i], base, zos);
            } else {
                FileInputStream in = null;
                try {
                    in = new FileInputStream(files[i]);
                    ZipEntry entry = new ZipEntry(files[i].getPath().substring(base.getPath().length() + 1));
                    zos.putNextEntry(entry);
                    while (-1 != (read = in.read(buffer))) {
                        zos.write(buffer, 0, read);
                    }
                    in.close();
                } catch (IOException e) {
                    throw e;
                } finally {
                    IOUtils.closeQuietly(in);
                }
            }
        }
    }

    public static final void unzip(File zip, File extractTo) throws IOException {
        ZipFile archive = null;
        try {
            archive = new ZipFile(zip);
            Enumeration<?> e = archive.entries();
            while (e.hasMoreElements()) {
                ZipEntry entry = (ZipEntry) e.nextElement();
                File file = new File(extractTo, entry.getName());
                if (entry.isDirectory()) {
                    if (!file.exists()) {
                        file.mkdirs();
                    }
                } else {
                    if (!file.getParentFile().exists()) {
                        file.getParentFile().mkdirs();
                    }

                    InputStream in = archive.getInputStream(entry);
                    BufferedOutputStream out = null;
                    try {
                        out = new BufferedOutputStream(new FileOutputStream(file));

                        byte[] buffer = new byte[BUFFER_SIZE];
                        int read;

                        while (-1 != (read = in.read(buffer))) {
                            out.write(buffer, 0, read);
                        }

                        in.close();
                        out.close();
                    } finally {
                        out.close();
                    }
                }
            }
            archive.close();
        } finally {
            if (archive!= null)
                archive.close();
        }
    }

    public static List<String> getListDocumentPath(File zip, File extractTo) throws IOException {
        List<String> res = new ArrayList<String>();
        ZipFile archive = new ZipFile(zip);
        Enumeration<?> e = archive.entries();
        while (e.hasMoreElements()) {
            String toAdd = extractTo.getAbsolutePath() + "/";
            ZipEntry entry = (ZipEntry) e.nextElement();
            toAdd = toAdd + entry.getName();
            res.add(toAdd);
        }
        archive.close();
        return res;
    }

    /**
     * copie le fichier source dans le fichier resultat retourne vrai si cela réussit
     */
    public static boolean copyFile(File source, File dest) {
        try {
            // Declaration et ouverture des flux
            java.io.FileInputStream sourceFile = new java.io.FileInputStream(source);

            try {
                java.io.FileOutputStream destinationFile = null;

                try {
                    destinationFile = new FileOutputStream(dest);

                    // Lecture par segment de 0.5Mo
                    byte buffer[] = new byte[512 * 1024];
                    int nbLecture;

                    while ((nbLecture = sourceFile.read(buffer)) != -1) {
                        destinationFile.write(buffer, 0, nbLecture);
                    }
                } finally {
                    destinationFile.close();
                }
            } finally {
                sourceFile.close();
            }
        } catch (IOException e) {
            log.error("", e);
            // Erreur
            return false;
        }
// Résultat OK
        return true;
    }

    public static void writeFile(String filename, String text) throws IOException {
        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(filename);
            fos.write(text.getBytes(StandardCharsets.UTF_8));
        } catch (IOException e) {
            fos.close();
            throw e;
        } finally {
            fos.close();
        }
    }

    public static boolean deleteDirectory(File path) {
        boolean resultat = true;

        if (path.exists()) {
            File[] files = path.listFiles();
            for (int i = 0; i < files.length; i++) {
                if (files[i].isDirectory()) {
                    resultat &= deleteDirectory(files[i]);
                } else {
                    resultat &= files[i].delete();
                }
            }
        }
        resultat &= path.delete();
        return (resultat);
    }
}