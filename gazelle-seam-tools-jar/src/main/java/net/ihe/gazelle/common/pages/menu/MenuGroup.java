package net.ihe.gazelle.common.pages.menu;

import java.util.List;

import net.ihe.gazelle.common.pages.Page;

public class MenuGroup extends MenuEntry {

	private List<Menu> children;

	public MenuGroup(Page page, List<Menu> children) {
		super(page);
		this.children = children;
	}

	@Override
	public List<Menu> getItems() {
		return children;
	}

}
