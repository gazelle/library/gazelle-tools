package net.ihe.gazelle.common.filter;

import net.ihe.gazelle.hql.HQLQueryBuilderInterface;

import java.util.Map;

public interface NoHQLQueryBuilder<T> extends HQLQueryBuilderInterface<T> {

    void addLike(String propertyName, String filterValue);
    void addOrder(String propertyName, boolean ascending);
    void addEq(String propertyName, Object dataItem);
    Object getId(T item);
    Map<String, Long> getPossibleCountValues(String propertyName);
}
