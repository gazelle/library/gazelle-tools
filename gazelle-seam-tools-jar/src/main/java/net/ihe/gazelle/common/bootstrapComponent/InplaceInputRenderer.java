package net.ihe.gazelle.common.bootstrapComponent;

/**
 * Created by jlabbe on 10/09/15.
 */

import org.richfaces.component.AbstractInplaceInput;
import org.richfaces.component.InplaceState;
import org.richfaces.renderkit.InplaceInputRendererBase;
import org.richfaces.renderkit.RenderKitUtils;
import org.richfaces.renderkit.RenderKitUtils.*;
import org.richfaces.renderkit.util.HtmlDimensions;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.context.ResponseWriter;
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;

import static org.richfaces.renderkit.RenderKitUtils.*;


public class
InplaceInputRenderer extends InplaceInputRendererBase {

    private static final RenderKitUtils.Attributes PASS_THROUGH_ATTRIBUTES22 = attributes()
            .generic("dir","dir")


            .generic("lang","lang")


            .generic("onclick","onclick","click")


            .generic("ondblclick","ondblclick","dblclick","ondblclick")


            .generic("onkeydown","onkeydown","keydown")


            .generic("onkeypress","onkeypress","keypress")


            .generic("onkeyup","onkeyup","keyup")


            .generic("onmousedown","onmousedown","mousedown")


            .generic("onmousemove","onmousemove","mousemove")


            .generic("onmouseout","onmouseout","mouseout")


            .generic("onmouseover","onmouseover","mouseover")


            .generic("onmouseup","onmouseup","mouseup")


            .generic("role","role")


            .generic("style","style")


            .generic("title","title")

            ;

    private static final RenderKitUtils.Attributes PASS_THROUGH_ATTRIBUTES23 = attributes()
            .generic("tabindex","tabindex")

            ;

    private static final RenderKitUtils.Attributes ATTRIBUTES_FOR_SCRIPT_HASH22 = attributes()
            .generic("state","state")


            .generic("saveOnBlur","saveOnBlur")
            .defaultValue(true)

            .generic("showControls","showControls")
            .defaultValue(false)
            ;

    private static final RenderKitUtils.Attributes ATTRIBUTES_FOR_SCRIPT_HASH23 = attributes()
            .generic("onbegin","onbegin")


            .generic("oncomplete","oncomplete")


            .generic("onerror","onerror")


            .generic("onbeforedomupdate","onbeforedomupdate")


            .generic("onselectitem","onselectitem")


            .generic("onchange","onchange","change")


            .generic("onblur","onblur","blur")


            .generic("onfocus","onfocus","focus")

            ;


    private static String convertToString(Object object)
    {
        return object != null ? object.toString() : "";
    }

    private static boolean isEqual(Object o1, Object o2)
    {
        if (o1 != null) {
            return o1.equals(o2);
        } else {
            //o1 == null
            return o2 == null;
        }
    }

    private static boolean convertToBoolean(Object object)
    {
        if (object == null) {
            return false;
        }

        if (object instanceof Boolean) {
            return (Boolean) object;
        }

        return Boolean.valueOf(object.toString());
    }

    @Override
    protected String getInputWidth(UIComponent component) {
        String value = ((AbstractInplaceInput) component).getInputWidth();
        if (value == null || "".equals(value)) {
            value = "auto";
        }
        return HtmlDimensions.formatSize(value);
    }

    @Override
    public void doEncodeEnd(ResponseWriter responseWriter, FacesContext facesContext, UIComponent component)
            throws IOException
    {
        String clientId = component.getClientId(facesContext);
        InplaceState inplaceState  =  (InplaceState)this.getInplaceState(component);
        String inplaceValue  =  (String)this.getValue(facesContext,component);
        String defaultLabel  =  (String)(((inplaceValue != null) && (inplaceValue.length() != 0)) ? inplaceValue : "\u00A0\u00A0\u00A0");
        responseWriter.startElement("span", component);
        {
            Object value = this.concatClasses(this.getContainerStyleClasses(component),component.getAttributes().get("styleClass"));
            if(null != value &&
                    shouldRenderAttribute(value)
                    ) {
                responseWriter.writeAttribute("class",value,null);
            }

        }

        {
            String value = clientId;
            if(null != value &&
                    value.length()>0
                    ) {
                responseWriter.writeAttribute("id",value,null);
            }

        }

        renderPassThroughAttributes(facesContext, component,
                PASS_THROUGH_ATTRIBUTES22);

        responseWriter.startElement("span", component);
        {
            String value = "rf-ii-lbl" + convertToString((isEqual(inplaceValue,component.getAttributes().get("defaultLabel")) ? " rf-ii-dflt-lbl" : ""));
            if(null != value &&
                    value.length()>0
                    ) {
                responseWriter.writeAttribute("class",value,null);
            }

        }

        {
            String value = convertToString(clientId) + "Label";
            if(null != value &&
                    value.length()>0
                    ) {
                responseWriter.writeAttribute("id",value,null);
            }

        }


        {
            Object text = defaultLabel;
            if (text != null) {
                responseWriter.writeText(text, null);
            }
        }

        responseWriter.endElement("span");
        if ((!convertToBoolean(component.getAttributes().get("disabled")))) {
            responseWriter.startElement("input", component);
            responseWriter.writeAttribute("class","rf-ii-none",null);

            {
                String value = convertToString(clientId) + "Focus";
                if(null != value &&
                        value.length()>0
                        ) {
                    responseWriter.writeAttribute("id",value,null);
                }

            }

            {
                String value = convertToString(clientId) + "Focus";
                if(null != value &&
                        value.length()>0
                        ) {
                    responseWriter.writeAttribute("name",value,null);
                }

            }

            responseWriter.writeAttribute("style","position: absolute; top: 0px; left: 0px; outline-style: none;",null);

            responseWriter.writeAttribute("tabindex","-1",null);

            responseWriter.writeAttribute("type","image",null);


            responseWriter.endElement("input");
            responseWriter.startElement("span", component);
            {
                String value = this.getEditStyleClass(component,inplaceState);
                if(null != value &&
                        value.length()>0
                        ) {
                    responseWriter.writeAttribute("class",value+" gzl-never-wrap gzl-inplace-btn",null);
                }

            }

            {
                String value = convertToString(clientId) + "Edit";
                if(null != value &&
                        value.length()>0
                        ) {
                    responseWriter.writeAttribute("id",value,null);
                }

            }


            responseWriter.startElement("input", component);
            responseWriter.writeAttribute("autocomplete","off",null);

            responseWriter.writeAttribute("class","rf-ii-fld",null);

            {
                String value = convertToString(clientId) + "Input";
                if(null != value &&
                        value.length()>0
                        ) {
                    responseWriter.writeAttribute("id",value,null);
                }

            }

            {
                String value = clientId;
                if(null != value &&
                        value.length()>0
                        ) {
                    responseWriter.writeAttribute("name",value,null);
                }

            }

            {
                String value = "width: " + convertToString(this.getInputWidth(component)) + ";";
                if (null != value &&
                        value.length() > 0
                        ) {
                    responseWriter.writeAttribute("style", value, null);
                }

            }

            responseWriter.writeAttribute("type","text",null);

            {
                String value = this.getInputValue(facesContext,component);
                if(null != value &&
                        value.length()>0
                        ) {
                    responseWriter.writeAttribute("value",value,null);
                }

            }

            renderPassThroughAttributes(facesContext, component,
                    PASS_THROUGH_ATTRIBUTES23);

            renderInputHandlers(facesContext, component);
            responseWriter.endElement("input");
            if (convertToBoolean(component.getAttributes().get("showControls"))) {
                responseWriter.startElement("span", component);
                responseWriter.writeAttribute("class","rf-ii-btn-prepos",null);


                responseWriter.startElement("span", component);
                responseWriter.writeAttribute("class","rf-ii-btn-pos",null);


                responseWriter.startElement("span", component);
                responseWriter.writeAttribute("class","rf-ii-btn-shdw",null);

                {
                    String value = convertToString(clientId) + "Btnshadow";
                    if(null != value &&
                            value.length()>0
                            ) {
                        responseWriter.writeAttribute("id",value,null);
                    }

                }

                responseWriter.startElement("span", component);
                responseWriter.writeAttribute("class","rf-ii-btn-cntr",null);

                {
                    String value = convertToString(clientId) + "Btn";
                    if(null != value &&
                            value.length()>0
                            ) {
                        responseWriter.writeAttribute("id",value,null);
                    }

                }


                responseWriter.startElement("span", component);
                responseWriter.writeAttribute("class","rf-ii-btn",null);

                {
                    String value = convertToString(clientId) + "Okbtn";
                    if(null != value &&
                            value.length()>0
                            ) {
                        responseWriter.writeAttribute("id",value,null);
                    }

                }

                {
                    String value = convertToString(clientId) + "Okbtn";
                    if(null != value &&
                            value.length()>0
                            ) {
                        responseWriter.writeAttribute("name",value,null);
                    }

                }

                responseWriter.writeAttribute("onmousedown","this.className='rf-ii-btn-p'",null);

                responseWriter.writeAttribute("onmouseout","this.className='rf-ii-btn'",null);

                responseWriter.writeAttribute("onmouseup","this.className='rf-ii-btn'",null);

                responseWriter.startElement("i", component);
                responseWriter.writeAttribute("class","gzl-icon-not-checked",null);
                responseWriter.endElement("i");

                responseWriter.endElement("span");
                responseWriter.startElement("span", component);
                responseWriter.writeAttribute("class","rf-ii-btn",null);

                {
                    String value = convertToString(clientId) + "Cancelbtn";
                    if(null != value &&
                            value.length()>0
                            ) {
                        responseWriter.writeAttribute("id",value,null);
                    }

                }

                {
                    String value = convertToString(clientId) + "Cancelbtn";
                    if(null != value &&
                            value.length()>0
                            ) {
                        responseWriter.writeAttribute("name",value,null);
                    }

                }

                responseWriter.writeAttribute("onmousedown","this.className='rf-ii-btn-press'",null);

                responseWriter.writeAttribute("onmouseout","this.className='rf-ii-btn'",null);

                responseWriter.writeAttribute("onmouseup","this.className='rf-ii-btn'",null);

                responseWriter.startElement("i", component);
                responseWriter.writeAttribute("class","gzl-icon-times-blue",null);
                responseWriter.endElement("i");
                responseWriter.endElement("span");

                responseWriter.endElement("span");
                responseWriter.endElement("span");
                responseWriter.endElement("span");
                responseWriter.endElement("span");
            }
            responseWriter.endElement("span");
            responseWriter.startElement("script", component);
            responseWriter.writeAttribute("type","text/javascript",null);


            Map<String, Object> options = new LinkedHashMap<String, Object>();
            addToScriptHash(options, "readyCss", this.concatClasses("rf-ii",component.getAttributes().get("readyStateClass")), null, null);

            addToScriptHash(options, "noneCss", this.concatClasses("rf-ii-none",component.getAttributes().get("noneStateClass")), null, null);

            addToScriptHash(options, "changedCss", this.concatClasses("rf-ii-chng",component.getAttributes().get("changedClass")), null, null);

            addToScriptHash(options, "editCss", this.concatClasses("rf-ii-act",component.getAttributes().get("activeClass")), null, null);

            addToScriptHash(options, "editEvent", this.getEditEvent(component), null, null);

            addToScriptHash(options, "defaultLabel", defaultLabel, null, null);

            addToScriptHash(options, facesContext, component, ATTRIBUTES_FOR_SCRIPT_HASH22, null);


            addToScriptHash(options, facesContext, component, ATTRIBUTES_FOR_SCRIPT_HASH23, ScriptHashVariableWrapper.eventHandler);

            {
                Object text = "new RichFaces.ui.InplaceInput(\"" + convertToString(clientId) + "\", " + convertToString(toScriptArgs(options)) + ");";
                if (text != null) {
                    responseWriter.writeText(text, null);
                }
            }

            responseWriter.endElement("script");
        }
        responseWriter.endElement("span");

    }
}

