package net.ihe.gazelle.common.filter;

import net.ihe.gazelle.common.filter.list.NoHQLDataModel;

import javax.faces.context.FacesContext;

public abstract class NoHQLFilterDataModel<T> extends NoHQLDataModel<T> {

    private static final long serialVersionUID = 385079253915019153L;
    private final transient NoHQLFilter<T> filter;

    protected NoHQLFilterDataModel(NoHQLFilter<T> filter) {
        this(filter, null);
    }

    protected NoHQLFilterDataModel(NoHQLFilter<T> filter, String dataTableId) {
        super(dataTableId);
        this.filter = filter;
        filter.setDataModel(this);
    }

    @Override
    protected void appendFilters(FacesContext context, NoHQLQueryBuilder<T> queryBuilder) {
        super.appendFilters(context, queryBuilder);
        filter.appendFilters(queryBuilder);
    }
}
