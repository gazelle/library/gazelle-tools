package net.ihe.gazelle.common;

import net.ihe.gazelle.translate.TranslateProvider;

import org.jboss.seam.international.StatusMessage;

public class SeamTranslateProvider implements TranslateProvider {

	@Override
	public String getTranslation(String key) {
		return StatusMessage.getBundleMessage(key, key);
	}

}
