package net.ihe.gazelle.common.pages;

import net.ihe.gazelle.common.servletfilter.CookieBrowserFilter;
import net.ihe.gazelle.common.util.GazelleCookie;
import net.ihe.gazelle.preferences.PreferenceService;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Observer;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.exception.ExceptionHandler;
import org.jboss.seam.exception.Exceptions;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.faces.Redirect;
import org.jboss.seam.international.StatusMessage;
import org.jboss.seam.security.AuthorizationException;
import org.jboss.seam.security.Identity;
import org.jboss.seam.security.NotLoggedInException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.faces.application.ViewExpiredException;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * This class manage Authorization exceptions and redirect users to the appropriate views.
 */
@Name("authzExceptionHandler")
@AutoCreate
@Scope(ScopeType.APPLICATION)
public class AuthzExceptionHandler extends ExceptionHandler {

    private static final Logger LOG = LoggerFactory.getLogger(AuthzExceptionHandler.class);
    private static final String LOGGED_ONCE = "LOGGED_ONCE";
    private static Map<String, String> storedLocations = Collections.synchronizedMap(new HashMap<String, String>());
    public AuthzExceptionHandler() {
        super();
    }

    @Observer("org.jboss.seam.postInitialization")
    public void initializeExceptionHandler() {
        if (LOG.isDebugEnabled()) {
            LOG.debug("initializeExceptionHandler");
        }

        // this handler will delegate some exceptions to us
        Exceptions.instance().getHandlers().add(0, this);
    }

    @Override
    public boolean isHandler(Exception e) {
        if (LOG.isDebugEnabled()) {
            LOG.debug("isHandler");
        }
        if ((e instanceof AuthorizationException) || (e instanceof NotLoggedInException)
                || (e instanceof ViewExpiredException)) {
            return true;
        }
        return false;
    }

    @Override
    public void handle(Exception e) {
        if (LOG.isDebugEnabled()) {
            LOG.debug("handle");
        }

        if (Identity.instance().isLoggedIn()  && (e instanceof AuthorizationException)) {
            restoreHome(StatusMessage
                    .getBundleMessage("net.ihe.gazelle.rights.needed", "net.ihe.gazelle.rights.needed"));
            return;
        }

        boolean forceLogin = false;
        if (GazelleCookie.getCookie(LOGGED_ONCE) != null || e instanceof NotLoggedInException) {
            forceLogin = true;
        }

        String lastServletPath;
        FacesContext facesContext = FacesContext.getCurrentInstance();
        HttpServletRequest request = (HttpServletRequest) facesContext.getExternalContext().getRequest();
        if (e instanceof ViewExpiredException) {
            lastServletPath = GazelleSecurityCheck.getServletURL(request);
        } else {
            lastServletPath = GazelleSecurityCheck.LAST_SERVLET_PATH.get();
        }
        if (forceLogin && !Identity.instance().isLoggedIn()) {
            String browserId = GazelleCookie.getCookie(CookieBrowserFilter.COOKIE_BROWSER_ID);
            if (lastServletPath != null) {
                storedLocations.put(browserId, lastServletPath);
            } else {
                storedLocations.put(browserId, null);
                FacesMessages.instance().add(StatusMessage.Severity.ERROR, "Unable to restore page viewed...", e);
            }

            // If CAS is enabled
            if (Boolean.TRUE.equals(PreferenceService.getBoolean("cas_enabled"))) {
                // Redirect users to CAS login page with correct request
                String redirectUrl = request.getContextPath();
                if (lastServletPath != null) {
                    try {
                        // Encode the URL to avoid problems with special characters
                        lastServletPath = URLEncoder.encode(lastServletPath, StandardCharsets.UTF_8.name());
                    } catch (UnsupportedEncodingException e1) {
                        LOG.error("Unable to encode URL", e1);
                    }
                    redirectUrl = request.getContextPath() + "/login/login.seam?request=" + lastServletPath;
                }

                restoreView(redirectUrl);
            } else {
                // Redirect to default anchor login page (used for ip login).
                Redirect redirectLogin = Redirect.instance();
                redirectLogin.setViewId("/login/ipLogin.seam");
                redirectLogin.execute();
            }
        } else {
            restoreView(lastServletPath);
        }

    }

    @Observer(Identity.EVENT_LOGIN_SUCCESSFUL)
    public void restoreViewIfNeeded() {
        if (LOG.isDebugEnabled()) {
            LOG.debug("restoreViewIfNeeded");
        }
        GazelleCookie.setCookie(LOGGED_ONCE, "true");
        String browserId = GazelleCookie.getCookie(CookieBrowserFilter.COOKIE_BROWSER_ID);
        String servletPath = storedLocations.get(browserId);
        storedLocations.put(browserId, null);
        restoreView(servletPath);
    }

    private void restoreView(String path) {
        if (path != null) {
            HttpServletResponse response = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext()
                    .getResponse();
            try {
                if (!response.isCommitted())
                    response.sendRedirect(path);
                FacesContext.getCurrentInstance().responseComplete();
            } catch (IOException e) {
                LOG.error("Failed to redirect to {}", path);
            }
        }
    }

    private void restoreHome(String message) {
        if (message != null) {
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, message);
        }
        Redirect redirectHome = Redirect.instance();
        redirectHome.setViewId("/home.xhtml");
        redirectHome.execute();
    }

}
