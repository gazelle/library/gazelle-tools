package net.ihe.gazelle.common.fineuploader;

import net.ihe.gazelle.preferences.PreferenceService;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.IOUtils;
import org.jboss.seam.Component;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Install;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.intercept.BypassInterceptors;
import org.jboss.seam.annotations.web.Filter;
import org.jboss.seam.security.Identity;
import org.jboss.seam.servlet.ContextualHttpServletRequest;
import org.jboss.seam.web.AbstractFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;

@Scope(ScopeType.APPLICATION)
@Name("fineuploaderFilter")
@Install
@BypassInterceptors
@Filter
public class FineuploaderFilter extends AbstractFilter {

	private static Logger log = LoggerFactory.getLogger(FineuploaderFilter.class);

	private static String CONTENT_TYPE = "text/plain";
	private static int RESPONSE_CODE = 200;

	private static File TEMP_DIR;
	private static final int TEMP_DIR_ATTEMPTS = 10000;

	public static File createTempDir() {
		File baseDir = new File(System.getProperty("java.io.tmpdir"));
		String baseName = System.currentTimeMillis() + "-";

		for (int counter = 0; counter < TEMP_DIR_ATTEMPTS; counter++) {
			File tempDir = new File(baseDir, baseName + counter);
			if (tempDir.mkdir()) {
				return tempDir;
			}
		}
		throw new IllegalStateException("Failed to create directory within " + TEMP_DIR_ATTEMPTS + " attempts (tried "
				+ baseName + "0 to " + baseName + (TEMP_DIR_ATTEMPTS - 1) + ')');
	}

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		super.init(filterConfig);
		TEMP_DIR = createTempDir();
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException,
			ServletException {
		if (request instanceof HttpServletRequest) {
			final HttpServletRequest httpRequest = (HttpServletRequest) request;
			final HttpServletResponse httpResponse = (HttpServletResponse) response;

			String servletPath = httpRequest.getServletPath();
			if ("/fineupload".equals(servletPath)) {
				new ContextualHttpServletRequest(httpRequest) {
					@Override
					public void process() throws Exception {
                        if (Identity.instance().isLoggedIn()) {
                            doWork(httpRequest, httpResponse);
                        } else {
                            writeResponse(httpResponse.getWriter(), "File cannot be uploaded, please refresh your page");
                        }
					}
				}.run();

			} else {
				chain.doFilter(request, response);
			}
		} else {
			chain.doFilter(request, response);
		}
	}

	protected void doWork(HttpServletRequest httpRequest, HttpServletResponse httpResponse) {
		RequestParser requestParser;

		FileOutputStream fos = null;
		InputStream tmpIs = null;

		try {
			httpResponse.setContentType(CONTENT_TYPE);
			httpResponse.setStatus(RESPONSE_CODE);

			// retrieve request infos
			boolean multiPart = ServletFileUpload.isMultipartContent(httpRequest);

			MultipartUploadParser multipartUploadParser = null;
			if (multiPart) {
				multipartUploadParser = new MultipartUploadParser(httpRequest, TEMP_DIR, getServletContext());
			}

			requestParser = RequestParser.getInstance(httpRequest, multipartUploadParser);

			// First copy file to a temp file
			File tmpFile = File.createTempFile("upload", "tmp");

			if (multiPart) {
				requestParser.getUploadItem().write(tmpFile);
			} else {
				fos = new FileOutputStream(tmpFile);
				tmpIs = httpRequest.getInputStream();
				IOUtils.copy(tmpIs, fos);
				tmpIs.close();
				fos.close();
			}

			sendToBean(tmpFile, requestParser);
			writeResponse(httpResponse.getWriter(), null);

		} catch (Exception e) {
			log.error("Problem handling upload request", e);
			try {
				writeResponse(httpResponse.getWriter(), e.getMessage());
			} catch (Exception e2) {
				log.error("Failed to send error...", e2);
			}
		} finally {
			IOUtils.closeQuietly(tmpIs);
			IOUtils.closeQuietly(fos);
		}
	}

	private void sendToBean(File tmpFile, RequestParser requestParser) throws IOException {
		Object bean = Component.getInstance(requestParser.getBeanName());
		if (bean instanceof FineuploaderListener) {
			FineuploaderListener fineuploaderListener = (FineuploaderListener) bean;
			fineuploaderListener.uploadedFile(tmpFile, requestParser.getFilename(), requestParser.getId(),
					requestParser.getParam());
		}
	}

	private void writeResponse(PrintWriter writer, String failureReason) {
		if (failureReason == null) {
			writer.print("{\"success\": true}");
		} else {
			writer.print("{\"error\": \"" + failureReason + "\"}");
		}
	}

	public int getMaxSize() {
		return PreferenceService.getInteger("upload_max_size");
	}

	@Override
	public void destroy() {
		// NOTHING
	}

}
