package net.ihe.gazelle.common.filter.list;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.reflect.FieldUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Array;
import java.util.Collection;

public class FieldProvider {

    private static Logger log = LoggerFactory.getLogger(FieldProvider.class);

    private FieldProvider() {
    }

    public static String getElementPropertyValueAsString(Object el, String prop) {
        String[] listSubProp = prop.split("\\.");
        try {
            Object aa = el;
            if (aa == null) {
                return null;
            }
            for (String subprop : listSubProp) {
                if (aa == null) {
                    return null;
                }
                if (subprop.matches("^[^\\[\\]]+\\[\\d+\\]$")) {
                    String fieldName = subprop.substring(0, subprop.indexOf('['));
                    Object subelement = FieldUtils.readField(aa, fieldName, true);
                    int index = Integer.valueOf(subprop.substring(subprop.indexOf('[') + 1, subprop.indexOf(']')));
                    if (subelement instanceof Collection) {
                        aa = CollectionUtils.get(subelement, index);
                    } else if (subelement.getClass().isArray()) {
                        aa = Array.get(subelement, index);
                    }
                } else {
                    aa = FieldUtils.readField(aa, subprop, true);
                }
            }
            return aa.toString();
        } catch (Exception e) {
            log.error("error to treat property : " + prop + " for Class : " + el.getClass());
        }
        return null;
    }
}
