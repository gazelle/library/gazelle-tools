package net.ihe.gazelle.common.filter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;

public class StringConverter implements Converter {

	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		if ("EMPTY-VALUE".equals(value)) {
			return "";
		}
		return value;
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component, Object value) {
		if ("".equals(value)) {
			return "EMPTY-VALUE";
		}
		return (String) value;
	}

}
