package net.ihe.gazelle.common.pages.menu;

import net.ihe.gazelle.common.pages.Page;
import net.ihe.gazelle.common.pages.PageCheckAuthorization;

import java.util.ArrayList;
import java.util.List;

public class MenuEntry implements Menu {

	private static final List<Menu> EMPTY = new ArrayList<Menu>();

	private Page page;

	public MenuEntry(Page page) {
		super();
		this.page = page;
	}

	@Override
	public Page getPage() {
		return page;
	}

	@Override
	public List<Menu> getItems() {
		return EMPTY;
	}

	@Override
	public boolean isVisible() {
		return PageCheckAuthorization.isGranted(page);
	}

}
