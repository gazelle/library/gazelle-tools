package net.ihe.gazelle.common.tag;

import com.lowagie.text.Font;
import com.lowagie.text.FontFactory;
import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.xml.simpleparser.EntitiesToUnicode;
import org.jboss.seam.pdf.ITextUtils;
import org.jboss.seam.pdf.ui.UIFont;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import java.io.File;
import java.io.IOException;

public class PDFFont extends UIFont {

    private static Logger log = LoggerFactory.getLogger(PDFFont.class);

    public static final String COMPONENT_TYPE = "net.ihe.gazelle.common.tag.PDFFont";

    Font font;

    int size = Font.UNDEFINED;
    String style;
    String color;
    boolean embedded = false;

    @Override
    public int getSize() {
        return (Integer) valueBinding("size", size);
    }

    @Override
    public void setSize(int size) {
        this.size = size;
    }

    @Override
    public void setStyle(String style) {
        this.style = style;
    }

    @Override
    public String getStyle() {
        return (String) valueBinding("style", style);
    }

    @Override
    public String getColor() {
        return (String) valueBinding("color", color);
    }

    @Override
    public void setColor(String color) {
        this.color = color;
    }

    @Override
    public boolean getEmbedded() {
        return (Boolean) valueBinding("embedded", embedded);
    }

    @Override
    public void setEmbedded(boolean embedded) {
        this.embedded = embedded;
    }

    @Override
    public Font getFont() {
        return font;
    }

    @Override
    public Object getITextObject() {
        return null;
        // we don't add to this component, so skip
    }

    @Override
    public void removeITextObject() {
        // font = null;
    }

    @Override
    public void createITextObject(FacesContext context) {
        boolean isJapanese = false;
        boolean isTurkish = false;
        for (UIComponent child : this.getChildren()) {
            if (!isJapanese && child.getFamily().equals("facelets.LiteralText")) {
                try {
                    String text = EntitiesToUnicode.decodeString(extractText(context, child));
                    if (containsJapanese(text)) {
                        isJapanese = true;
                        log.info("isJapanese");
                    } else if (containsTurkish(text)) {
                        isTurkish = true;
                        log.info("isTurkish");
                    }
                } catch (IOException e) {
                    // failed to get content
                    log.error("", e.getMessage());
                }
            }
        }

        if (isJapanese) {
            font = FontFactory.getFont("HeiseiKakuGo-W5", "UniJIS-UCS2-H", getEmbedded(), getSize());
            log.info("font japanese");
        } else if (isTurkish) {
            if (new File("/opt/gazelle/fonts/PTC55F.ttf").exists()) {
                FontFactory.register("/opt/gazelle/fonts/PTC55F.ttf", "PT-San");
                font = FontFactory.getFont("PT-San", BaseFont.IDENTITY_H, true, getSize());
                log.info("font turkish");
            } else {
                log.error("Failed to use Turkish language. PTC55F.ttf is not installed in /opt/gazelle/fonts/");
            }
        } else {
            try {
                font = FontFactory.getFont(null, getSize());
                log.info("font other");
            } catch (Exception e) {
                log.error(e.getMessage());
            }
        }
        if (getStyle() != null) {
            font.setStyle(getStyle());
        }
        if (getColor() != null) {
            font.setColor(ITextUtils.colorValue(getColor()));
        }
    }

    public boolean containsJapanese(String s) {
        for (int i = 0; i < s.length(); ) {
            int codepoint = s.codePointAt(i);
            i += Character.charCount(codepoint);
            if (Character.UnicodeScript.of(codepoint) == Character.UnicodeScript.HAN) {
                return true;
            }

        }
        return false;
    }

    public boolean containsCyrillic(String s) {
        for (int i = 0; i < s.length(); ) {
            int codepoint = s.codePointAt(i);
            i += Character.charCount(codepoint);
            if (Character.UnicodeScript.of(codepoint) == Character.UnicodeScript.CYRILLIC) {
                return true;
            }

        }
        return false;
    }

    public boolean containsTurkish(String s) {
        for (int i = 0; i < s.length(); i++) {
            if (isTurkish(s.charAt(i))) {
                return true;
            }
        }
        return false;
    }

    // Read more: How to Detect a CJK Character in Java | eHow.com
    // http://www.ehow.com/how_11383680_detect-cjk-character-java.html#ixzz1kwduSuT8
    public boolean isJapanese(char c) {
        // simpler:
        return c > '\u00ff';
    }

    public boolean isTurkish(char c) {
        char[] charList = {'\u011f', '\u011e', '\u0131', '\u00f6', '\u00d6', '\u00fc', '\u00dc', '\u015f', '\u015e', '\u00e7', '\u00c7'};
        for (Character character : charList) {
            if (character.equals(c)) {
                log.info(Character.toString(c));
                return true;
            }
        }
        return false;
    }

    @Override
    public void handleAdd(Object o) {
        addToITextParent(o);
    }
}
