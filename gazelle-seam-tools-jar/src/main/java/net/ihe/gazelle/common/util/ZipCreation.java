package net.ihe.gazelle.common.util;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Enumeration;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipOutputStream;

/**
 * @author aboufahja, ceoche
 */
public class ZipCreation {

    private static Logger log = LoggerFactory.getLogger(ZipCreation.class);

    private static final int BUFFER = 2048;

    private ZipCreation() {
    }

    /**
     * Unzip the given archive at the current location
     *
     * @param pathzip Path to the zip archive to extract
     * @throws IOException in case of archive access error
     */
    public static void Unzip(String pathzip) throws IOException {
        ZipFile zipfile = null;
        BufferedInputStream is = null;
        FileOutputStream fos = null;
        BufferedOutputStream bos = null;
        try {
            zipfile = new ZipFile(pathzip);
            Enumeration<? extends ZipEntry> e = zipfile.entries();
            while (e.hasMoreElements()) {
                ZipEntry entry = e.nextElement();
                is = new BufferedInputStream(zipfile.getInputStream(entry));
                int count;
                byte data[] = new byte[BUFFER];
                fos = new FileOutputStream(entry.getName());
                bos = new BufferedOutputStream(fos, BUFFER);
                while ((count = is.read(data, 0, BUFFER)) != -1) {
                    bos.write(data, 0, count);
                }
                bos.flush();
                bos.close();
                fos.close();
                is.close();
            }
            zipfile.close();
        } catch (IOException e) {
            log.error("Error while unzipping", e);
            throw e;
        } finally {
            IOUtils.closeQuietly(bos);
            IOUtils.closeQuietly(fos);
            IOUtils.closeQuietly(is);
            IOUtils.closeQuietly(zipfile);
        }
    }

    /**
     * Zip the listed files into a new archive. The directory structure will be recreated.
     * The output zip file will not be created in case of error.
     *
     * @param pathzip   location where to create the new archive.
     * @param listFiles list of files to add to the zip.
     */
    public static void files2zip(String pathzip, List<String> listFiles) {
        byte[] zippedContent = files2zip(listFiles);
        if (zippedContent != null) {
            FileOutputStream fileOutputStream = null;
            try {
                fileOutputStream = new FileOutputStream(pathzip);
                fileOutputStream.write(zippedContent);
            } catch (Exception e) {
                log.error("Error while creating zip file", e);
            } finally {
                IOUtils.closeQuietly(fileOutputStream);
            }
        }
    }

    /**
     * Zip the listed files into a new archive. The directory structure will be recreated.
     *
     * @param listFiles list of files to add to the zip
     * @return the zip archive in a byte array or null if any error occurs.
     */
    public static byte[] files2zip(List<String> listFiles) {

        if (listFiles != null) {

            ByteArrayOutputStream byteArrayOutputStream = null;
            ZipOutputStream zipOutputStream = null;
            try {
                byteArrayOutputStream = new ByteArrayOutputStream();
                zipOutputStream = new ZipOutputStream(byteArrayOutputStream);

                for (String filePath : listFiles) {

                    FileInputStream fileInputStream = null;

                    try {
                        fileInputStream = new FileInputStream(filePath);
                        ZipEntry entry = new ZipEntry(filePath);
                        zipOutputStream.putNextEntry(entry);

                        int count;
                        byte data[] = new byte[BUFFER];
                        while ((count = fileInputStream.read(data, 0, BUFFER)) != -1) {
                            zipOutputStream.write(data, 0, count);
                        }

                        zipOutputStream.closeEntry();

                    } catch (IOException e) {
                        throw e;
                    } finally {
                        IOUtils.closeQuietly(fileInputStream);
                    }

                }

                zipOutputStream.close();
                return byteArrayOutputStream.toByteArray();

            } catch (IOException e) {
                log.error("Error while zipping", e);
            } finally {
                IOUtils.closeQuietly(byteArrayOutputStream);
                IOUtils.closeQuietly(zipOutputStream);
            }
        }

        return null;
    }
}