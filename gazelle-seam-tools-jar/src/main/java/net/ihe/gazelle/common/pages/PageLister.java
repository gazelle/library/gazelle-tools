package net.ihe.gazelle.common.pages;

import java.util.Collection;

public interface PageLister {

	Collection<Page> getPages();

}
