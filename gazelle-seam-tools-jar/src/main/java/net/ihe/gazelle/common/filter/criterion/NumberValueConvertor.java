package net.ihe.gazelle.common.filter.criterion;

import net.ihe.gazelle.hql.criterion.IntervalType;
import net.ihe.gazelle.hql.criterion.number.NumberValue;
import org.apache.commons.lang.StringUtils;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.faces.Converter;
import org.jboss.seam.annotations.intercept.BypassInterceptors;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import java.io.Serializable;

@BypassInterceptors
@Name("numberValueConvertor")
@Converter(forClass = NumberValue.class)
public class NumberValueConvertor implements javax.faces.convert.Converter, Serializable {

    private static final long serialVersionUID = 8477851468068494783L;

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        NumberValue result = new NumberValue(0, 0);
        if (value != null) {
            String[] split = StringUtils.split(value, '|');

            result.setIntervalType(IntervalType.valueOf(split[0]));
            result.setValue1(Integer.valueOf(split[1]));
            result.setValue2(Integer.valueOf(split[2]));
            result.setMinValue(Integer.valueOf(split[3]));
            result.setMaxValue(Integer.valueOf(split[4]));
        }
        return result;
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        if ((value != null) && (value instanceof NumberValue)) {
            NumberValue numberValue = (NumberValue) value;
            return numberValue.getIntervalType().name() + "|" + numberValue.getValue1() + "|" + numberValue.getValue2()
                    + "|" + numberValue.getMinValue() + "|" + numberValue.getMaxValue();
        }
        return null;
    }
}
