package net.ihe.gazelle.common.bootstrapComponent;

import org.richfaces.renderkit.ControlsState;
import org.richfaces.renderkit.DataScrollerBaseRenderer;
import org.richfaces.renderkit.HtmlConstants;
import org.richfaces.renderkit.RenderKitUtils;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.context.ResponseWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static org.richfaces.renderkit.RenderKitUtils.*;


public class DataScroller extends DataScrollerBaseRenderer {

    private static final RenderKitUtils.Attributes PASS_THROUGH_ATTRIBUTES13 = attributes()
            .generic("title", "title");


    private static String convertToString(Object object) {
        return object != null ? object.toString() : "";
    }

    @Override
    public void doEncodeEnd(ResponseWriter responseWriter, FacesContext facesContext, UIComponent component)
            throws IOException {
        String clientId = component.getClientId(facesContext);
        ControlsState controlsState = this.getControlsState(facesContext, component);
        Object style = component.getAttributes().get("style");
        Object styleClass = component.getAttributes().get("styleClass");
        responseWriter.startElement("nav", component);
        responseWriter.startElement("ul", component);
        {
            String value = "pagination " + convertToString(styleClass);
            if (null != value &&
                    value.length() > 0
                    ) {
                responseWriter.writeAttribute("class", value, null);
            }

        }

        {
            String value = clientId;
            if (null != value &&
                    value.length() > 0
                    ) {
                responseWriter.writeAttribute("id", value, null);
            }

        }

        {
            Object value = (this.shouldRender(component) ? style : "display:none");
            if (null != value &&
                    shouldRenderAttribute(value)
                    ) {
                responseWriter.writeAttribute("style", value, null);
            }

        }

        renderPassThroughAttributes(facesContext, component,
                PASS_THROUGH_ATTRIBUTES13);

        if (this.shouldRender(component)) {
            if (controlsState.getFirstRendered()) {
                boolean isEnabled = controlsState.getFirstEnabled();
                String id = convertToString(clientId) + "_ds_f";
                String defaultText = "\u00AB\u00AB\u00AB\u00AB";

                bootstrapControls(responseWriter, component, isEnabled, id, defaultText);
            }
            if (controlsState.getFastRewindRendered()) {
                boolean isEnabled = controlsState.getFastRewindEnabled();
                String id = convertToString(clientId) + "_ds_fr";
                String defaultText = "\u00AB\u00AB";
                bootstrapControls(responseWriter, component, isEnabled, id, defaultText);
            }
            if (controlsState.getPreviousRendered()) {
                boolean isEnabled = controlsState.getPreviousEnabled();
                String id = convertToString(clientId) + "_ds_prev";
                String defaultText = "\u00AB";

                bootstrapControls(responseWriter, component, isEnabled, id, defaultText);

            }
            Map digitals = (Map) this.renderPager(responseWriter, facesContext, component);
            if (controlsState.getNextRendered()) {
                boolean isEnabled = controlsState.getNextEnabled();
                String id = convertToString(clientId) + "_ds_next";
                String defaultText = "\u00BB";
                bootstrapControls(responseWriter, component, isEnabled, id, defaultText);
            }
            if (controlsState.getFastForwardRendered()) {
                boolean isEnabled = controlsState.getFastForwardEnabled();
                String id = convertToString(clientId) + "_ds_ff";
                String defaultText = "\u00BB\u00BB";
                bootstrapControls(responseWriter, component, isEnabled, id, defaultText);

            }
            if (controlsState.getLastRendered()) {
                boolean isEnabled = controlsState.getLastEnabled();
                String id = convertToString(clientId) + "_ds_l";
                String defaultText = "\u00BB\u00BB\u00BB\u00BB";

                bootstrapControls(responseWriter, component, isEnabled, id, defaultText);
            }
            Map buttons = (Map) this.getControls(facesContext, component, controlsState);
            responseWriter.startElement("script", component);
            responseWriter.writeAttribute("type", "text/javascript", null);


            buildScript(responseWriter, facesContext, component, buttons, digitals);

            responseWriter.endElement("script");
        }
        responseWriter.endElement("ul");
        responseWriter.endElement("nav");

    }

    private void bootstrapControls(ResponseWriter responseWriter, UIComponent component, boolean isEnabled, String id, String defaultText) throws IOException {
        responseWriter.startElement("li", component);
        if (!isEnabled) {
            responseWriter.writeAttribute("class", "disabled", null);
        }
        responseWriter.startElement("a", component);

        if (isEnabled) {
            responseWriter.writeURIAttribute("href", "javascript:void(0);", null);
        } else {
            responseWriter.writeURIAttribute("href", "#", null);
        }
        {
            String value = id;
            if (null != value &&
                    value.length() > 0
                    ) {
                responseWriter.writeAttribute("id", value, null);
            }

        }

        responseWriter.startElement("span", component);
        responseWriter.writeAttribute("aria-hidden", "true", null);
        responseWriter.writeText(defaultText, null);
        responseWriter.endElement("span");
        responseWriter.endElement("a");
        responseWriter.endElement("li");
    }

    @Override
    public Map<String, String> renderPager(ResponseWriter out, FacesContext context, UIComponent component) throws IOException {

        int currentPage = (Integer) component.getAttributes().get("page");
        int maxPages = (Integer) component.getAttributes().get("maxPagesOrDefault");
        int pageCount = (Integer) component.getAttributes().get("pageCount");

        Map<String, String> digital = new HashMap<String, String>();

        if (pageCount <= 1) {
            return digital;
        }

        int delta = maxPages / 2;

        int pages;
        int start;

        if (pageCount > maxPages && currentPage > delta) {
            pages = maxPages;
            start = currentPage - pages / 2 - 1;
            if (start + pages > pageCount) {
                start = pageCount - pages;
            }
        } else {
            pages = pageCount < maxPages ? pageCount : maxPages;
            start = 0;
        }

        String clientId = component.getClientId(context);

        int size = start + pages;
        for (int i = start; i < size; i++) {

            boolean isCurrentPage = (i + 1 == currentPage);
            String styleClass;
            String style;


            out.startElement(HtmlConstants.LI_ELEMENT, component);

            if (isCurrentPage) {
                out.writeAttribute(HtmlConstants.CLASS_ATTRIBUTE, "active", null);
            }
            out.startElement(HtmlConstants.A_ELEMENT, component);
            if (!isCurrentPage) {
                out.writeAttribute(HtmlConstants.HREF_ATTR, "javascript:void(0);", null);
            }


            String page = Integer.toString(i + 1);
            String id = clientId + "_ds_" + page;

            out.writeAttribute(HtmlConstants.ID_ATTRIBUTE, id, null);

            digital.put(id, page);

            out.writeText(page, null);
            out.endElement(HtmlConstants.A_ELEMENT);
            out.endElement(HtmlConstants.LI_ELEMENT);
        }

        return digital;
    }
}


