# Gazelle Tools

Gazelle tools is a set of libraries used by Gazelle Test Bed. It embeds:

* [Authorizations](gazelle-seam-tools-jar/README.md#authorizations)
* [Menu (UI and service)](gazelle-seam-tools-jar/README.md#build-the-top-menu)
  and [web page restrictions](gazelle-seam-tools-jar/README.md#pages-access-control)
* Filters and Pagination (Data scroller).
* File download and upload
* PDF writer and presenter UI component
* CSV and Excel export
* Various UI Components
    * Calendar and date
    * Inplace input
    * Number spinner input
    * Modal
    * Image link
    * Tree
    * Labels
    * Safe HTML renderer
* Interface services
    * UserService to get identity (deprecated, use `GazelleIdentity` from __sso-client-v7__)
    * Message publishing
    * i18n translation
    * Persistence Unit provider
* Technical components
    * [Data migration](gazelle-migration/README.md)
    * Cache manager
    * HTTP security filters
    * Java interface generator
    * Profiling and metrics
* Various other utils
    * Zip
    * Posix file permissions
    * Encryption
    * Thumbnail
    * URL

__Gazelle Tools__ requires JDK-8 to build and __JDK-7 with a Seam 2.3 container to run__.

## Build

To build, run all tests and package

```shell
mvn clean install
```

To run tests, __Gazelle Migration__ requires a running instance of PostgreSQL
on `localhost:5432` with user `gazelle` and password `gazelle`. Tests will create and drop several
times a database called `migration-test`.

Migration tests can be skipped :

```shell
mvn clean install -DskipITs
```