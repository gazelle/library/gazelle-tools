package net.ihe.gazelle.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static org.junit.Assert.assertEquals;

public class URLParserTest {

    private static final Logger LOG = LoggerFactory.getLogger(URLParserTest.class);

    @org.junit.Test
    public void replaceParams() {
        String url = "https://gazelle.ihe.net/gazelle-webservice-tester/rest/xdstools?testInstanceId=$$testInstanceId$$&callingToolOid" +
                "=$$callingToolOid$$";

        URLParser parser = new URLParser();
        url = parser.replaceParams(url, "1234", "1.3.6.1.4.1.12559.11.1.5.11");
        assertEquals("https://gazelle.ihe.net/gazelle-webservice-tester/rest/xdstools?testInstanceId=1234&callingToolOid" +
                "=1.3.6.1.4.1.12559.11.1.5.11", url);
        assertEquals("https://gazelle.ihe.net/gazelle-webservice-tester/rest/xdstools?testInstanceId=1234&amp;" +
                "callingToolOid=1.3.6.1.4.1.12559.11.1.5.11", new HTMLFilter().filter(url));
    }
}