package net.ihe.gazelle.messaging;

import java.util.List;

import net.ihe.gazelle.services.GenericServiceLoader;

public class MessagingService {

	public static void publishMessage(Object message) {
		List<MessagingProvider> providers = GenericServiceLoader.getServices(MessagingProvider.class);
		for (MessagingProvider messagingProvider : providers) {
			messagingProvider.receiveMessage(message);
		}
	}

	private MessagingService() {
		super();
	}

}
