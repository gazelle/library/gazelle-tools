package net.ihe.gazelle.messaging;

public class MessagePropertyChanged<U, V> {

	private U object;

	private String propertyName;

	private V oldValue;

	private V newValue;

	public MessagePropertyChanged(U object, String propertyName, V oldValue, V newValue) {
		super();
		this.object = object;
		this.propertyName = propertyName;
		this.oldValue = oldValue;
		this.newValue = newValue;
	}

	public U getObject() {
		return object;
	}

	public void setObject(U object) {
		this.object = object;
	}

	public String getPropertyName() {
		return propertyName;
	}

	public void setPropertyName(String propertyName) {
		this.propertyName = propertyName;
	}

	public V getOldValue() {
		return oldValue;
	}

	public void setOldValue(V oldValue) {
		this.oldValue = oldValue;
	}

	public V getNewValue() {
		return newValue;
	}

	public void setNewValue(V newValue) {
		this.newValue = newValue;
	}

}
