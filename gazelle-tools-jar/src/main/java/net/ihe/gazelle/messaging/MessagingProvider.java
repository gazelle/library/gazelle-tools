package net.ihe.gazelle.messaging;

public interface MessagingProvider {

	void receiveMessage(Object message);

}
