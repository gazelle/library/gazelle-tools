package net.ihe.gazelle.translate;

public interface TranslateProvider {

	String getTranslation(String key);

}
