package net.ihe.gazelle.users;

import java.util.Locale;

/**
 * @deprecated Instead, inject GazelleIdentity from sso-client-v7 library.
 */
@Deprecated
public interface UserProvider {

	String getUsername();

	boolean hasRole(String roleName);

	Locale getLocale();

}
