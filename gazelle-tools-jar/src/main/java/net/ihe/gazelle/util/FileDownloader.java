/*
 * Copyright 2008 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.ihe.gazelle.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;

/**
 * <b>Class Description : </b>FileDownloader<br>
 * <br>
 * This class describes the FileDownloader object which is used to manage all
 * the downloading operations used by Gazelle (eg. download a remote file,
 * etc...)
 * <p>
 * This class belongs to the Common-ejb module.
 *
 * @author Jean-Renan Chatel / INRIA Rennes IHE development Project
 * @version 1.0 - 2008, Dec 12
 * @class FileDownloader.java
 * @package net.ihe.gazelle.common.util

 */

public class FileDownloader {

    public static final int BUFFER_SIZE = 1024;
    public static String UNREACHABLE_URL = "UNREACHABLE_URL";
    public static String CONTENT_TYPE_ISSUE_FOR_URL = "CONTENT_TYPE_ISSUE_FOR_URL";
    public static String VALID_URL = "VALID_URL";

    private static final Logger log = LoggerFactory.getLogger(FileDownloader.class);

    /**
     * Constructor
     */
    public FileDownloader() {

    }

    /**
     * Method used to download a file (eg. Integration Statement PDF file)
     *
     * @param String fileToDownloadURL : URL used to download the Integration
     *               Statement file
     * @param String gazelleStoragePath : Path used to store the downloaded file
     *
     * @return File : Downloaded file object
     */
    public static File downloadFile(String fileToDownloadURL, String gazelleStoragePath) {
        InputStream input = null;
        FileOutputStream writeFile = null;
        File file = null;
        File localFile = null;

        try {

            if (!pingWebsite(fileToDownloadURL)) {
                log.error("Unreachable URL or file.");
                return null;
            }

            URL url = new URL(fileToDownloadURL);

            URLConnection connection = url.openConnection();

            int fileLength = connection.getContentLength();

            if (fileLength == -1) {
                log.error("Invalide URL or file.");
                return null;
            }

            input = connection.getInputStream();
            String fileName = gazelleStoragePath + url.getFile().substring(url.getFile().lastIndexOf('/') + 1);

            writeFile = new FileOutputStream(fileName);
            byte[] buffer = new byte[BUFFER_SIZE];
            int read;

            while ((read = input.read(buffer)) > 0) {
                writeFile.write(buffer, 0, read);
            }
            writeFile.flush();
            //

            localFile = new File(fileName);

            localFile.canRead();
            localFile.canWrite();

            return localFile;
        } catch (IOException e) {
            log.warn("Warning in downloadFile (URL = " + fileToDownloadURL + " ) Message = " + e.getMessage());

            // ExceptionLogging.logException(e, log);
        } finally {
            try {
                writeFile.close();
                input.close();
            } catch (IOException e) {
                log.error("Error in downloadFile - writeFile : IOException - Message = " + e.getMessage());
                // ExceptionLogging.logException(e, log);
            }
        }

        return localFile;
    }

    /**
     * Method used to download a file (eg. Integration Statement PDF file)
     *
     * @param String fileToDownloadURL : URL used to download the Integration
     *               Statement file
     * @param String gazelleStoragePath : Path used to store the downloaded file
     *
     * @return File : Downloaded file object
     */
    public static Pair<String, File> downloadIISFile(String fileToDownloadURL, String gazelleStoragePath) {
        InputStream input = null;
        FileOutputStream writeFile = null;
        File file = null;
        File localFile = null;
        URLConnection connection = null;
        URL url = null;

        try {

            url = new URL(fileToDownloadURL);

            connection = url.openConnection();

            int fileLength = connection.getContentLength();

            if (fileLength == -1) {
                log.error("Invalide URL or file.");
                return new Pair<String, File>(UNREACHABLE_URL, null);
            }

            if ((connection.getContentType().contains("/html")) || (connection.getContentType().contains("audio/"))
                    || (connection.getContentType().contains("video/"))) {
                log.warn("downloadIISFile - Content type issue (" + connection.getContentType()
                        + ") trying to download an IIS : " + fileToDownloadURL);
                return new Pair<String, File>(CONTENT_TYPE_ISSUE_FOR_URL, null);
            }
        } catch (IOException e) {
            log.warn("Warning in downloadIISFile - Content type issue (URL = " + fileToDownloadURL + " ) Message = "
                    + e.getMessage());
            return new Pair<String, File>(UNREACHABLE_URL, null);
            // ExceptionLogging.logException(e, log);
        }

        try {
            input = connection.getInputStream();
            String fileName = gazelleStoragePath + url.getFile().substring(url.getFile().lastIndexOf('/') + 1);

            writeFile = new FileOutputStream(fileName);
            byte[] buffer = new byte[BUFFER_SIZE];
            int read;

            while ((read = input.read(buffer)) > 0) {
                writeFile.write(buffer, 0, read);
            }
            writeFile.flush();

            localFile = new File(fileName);

            localFile.canRead();
            localFile.canWrite();

            return new Pair<String, File>(VALID_URL, localFile);
        } catch (IOException e) {
            log.warn("Warning in downloadIISFile (URL = " + fileToDownloadURL + " ) Message = " + e.getMessage());
            // ExceptionLogging.logException(e, log);
        } finally {
            try {
                writeFile.close();
                input.close();
            } catch (IOException e) {
                log.error("Error in downloadFile - writeFile : IOException - Message = " + e.getMessage());
                // ExceptionLogging.logException(e, log);
            }
        }

        return new Pair<String, File>(VALID_URL, localFile);
    }

    /**
     * Method used to ping a file, or check that a URL is valid (eg. Integration
     * Statement PDF file)
     *
     * @param String        fileToDownloadURL : URL used to download the Integration
     *                      Statement file
     * @param EntityManager entityManager : object managing Persistence layer
     *
     * @return String : Flag if the file is reachable
     */
    public static Boolean pingFile(String fileToDownloadURL) {
        try {

            URL url = new URL(fileToDownloadURL);
            URLConnection connection = url.openConnection();

            int fileLength = connection.getContentLength();

            if (fileLength == -1) {
                log.warn("pingFile() - Invalide URL or file " + fileToDownloadURL);
                return false;
            }

            return true;
        } catch (IOException e) {
            log.warn("Warning while trying to download the file " + fileToDownloadURL + " - Message = "
                    + e.getMessage());
            // Do not need to render exception here :
            // ExceptionLogging.logException(e, log);
            return null;
        }
    }

    /**
     * Method used to ping a file, or check that a URL is valid (eg. Integration
     * Statement PDF file)
     *
     * @param String        fileToDownloadURL : URL used to download the Integration
     *                      Statement file
     * @param EntityManager entityManager : object managing Persistence layer
     *
     * @return String : Flag - UNREACHABLE_URL if the file is reachable -
     * CONTENT_TYPE_ISSUE_FOR_URL if the contenttype of this file is not
     * DOC, RTF or PDFfile is reachable - VALID_URL if the file is
     * conform
     */
    public static String pingIISFile(String fileToDownloadURL) {

        try {

            URL url = new URL(fileToDownloadURL);
            URLConnection connection = url.openConnection();

            int fileLength = connection.getContentLength();

            if (fileLength == -1) {
                log.warn("pingFile() - Invalide URL or file " + fileToDownloadURL);
                return UNREACHABLE_URL;
            }

            if ((connection.getContentType().contains("/html")) || (connection.getContentType().contains("audio/"))
                    || (connection.getContentType().contains("video/"))) {
                log.warn("pingIISFile - Content type issue (" + connection.getContentType()
                        + ") trying to download an IIS : " + fileToDownloadURL);
                return CONTENT_TYPE_ISSUE_FOR_URL;
            } else {
                return VALID_URL;
            }
        } catch (IOException e) {
            log.warn("Warning in pingIISFile while trying to download the file " + fileToDownloadURL + " - Message = "
                    + e.getMessage());
            // Do not need to render exception here :
            // ExceptionLogging.logException(e, log);
            return UNREACHABLE_URL;
        }
    }

    /**
     * Method used to ping a website, or check that a URL is valid
     *
     * @param String URL : URL used to check
     *
     * @return Boolean : Flag indicating if the website is reachable
     */
    public static Boolean pingWebsite(String urlToPing) {
        InputStream input = null;

        try {

            URL url = new URL(urlToPing);
            URLConnection connection = url.openConnection();

            input = connection.getInputStream();

            return true;
        } catch (IOException e) {
            log.warn("Warning in pingWebsite while trying to ping the site. The URL " + urlToPing
                    + " is not reachable ! Message = " + e.getMessage());
            return false;
        }
    }

    /**
     * Method used to download a file (eg. Integration Statement PDF file)
     *
     * @param String        fileToDownloadURL : URL used to download the Integration
     *                      Statement file
     * @param String        gazelleStoragePath : Path used to store the downloaded file
     * @param EntityManager entityManager : object managing Persistence layer
     *
     * @return File : Downloaded file object
     */
    public static Boolean validateURL(String url, EntityManager entityManager) {

        Boolean result = false;

        if (url.startsWith("http://") || url.startsWith("ftp://") || url.startsWith("https://")) {
            result = true;
        }

        return result;
    }
}
