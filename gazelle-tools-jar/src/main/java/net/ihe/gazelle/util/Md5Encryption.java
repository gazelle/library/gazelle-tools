/*
 * Copyright 2008 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.ihe.gazelle.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * <b>Class Description : </b>Md5Encryption - Authentication Module<br>
 * <br>
 * This class manage the MD5 encryption for Gazelle application. This class is
 * helpful to encrypt safely : <li>password stored in the database</li> <li>
 * etc...</li>
 *
 * @author Jean-Renan Chatel / INRIA Rennes IHE development Project
 * @version 1.0 - 2008, April 28
 * @class Md5Encryption.java
 * @package net.ihe.gazelle.common.authentication

 */
public class Md5Encryption {
    public static final int BUFFER_SIZE = 8192;
    // ~ Methods
    // ///////////////////////////////////////////////////////////////////////////////////////////////////////

    private static Logger log = LoggerFactory.getLogger(Md5Encryption.class);

    /**
     * This method encrypts a string (eg. password) using MD5 algorithm. MD5 is
     * a secured encryption.
     *
     * @param passwordToEncrypt : This is the password we need to encrypt with MD5
     *
     * @return hashword : corresponds to the encrypted password using MD5
     * algorithm
     */
    public static String hashPassword(final String passwordToEncrypt) {
        String hashword = null;

        try {
            MessageDigest md5 = MessageDigest.getInstance("MD5");
            md5.update(passwordToEncrypt.getBytes());

            BigInteger hash = new BigInteger(1, md5.digest());
            hashword = hash.toString(16);
        } catch (final NoSuchAlgorithmException nsae) {
            log.error("failed!", nsae);
        }

        return hashword;
    }

    /**
     * This method calculate the MD5 checksum of a file (eg. Integration
     * Statement) using MD5 algorithm. MD5 is a secured encryption.
     *
     * @param fileToBeChecked : This is the file we need to check with MD5
     *
     * @return String md5Checksum : corresponds to the checksum of a file using
     * MD5 algorithm
     */
    public static String calculateMD5ChecksumForFile(final File fileToBeChecked) {
        String md5Checksum = "";
        try {
            InputStream is = new FileInputStream(fileToBeChecked);
            md5Checksum = calculateMD5ChecksumForInputStream(is);
        } catch (Exception e) {
            log.error("failed!", e);
        }

        return md5Checksum;
    }

    public static String calculateMD5ChecksumForInputStream(InputStream is) throws Exception {
        MessageDigest digest = MessageDigest.getInstance("MD5");
        String md5Checksum;
        byte[] buffer = new byte[BUFFER_SIZE];
        int read = 0;
        while ((read = is.read(buffer)) > 0) {
            digest.update(buffer, 0, read);
        }
        byte[] md5sum = digest.digest();
        BigInteger bigInt = new BigInteger(1, md5sum);
        md5Checksum = bigInt.toString(16);
        return md5Checksum;
    }

    /**
     * Testing method !! this method is just testing md5 checksum for different
     * files... This is way a to be sure that computation is well done and our
     * results are right.
     *
     * Here are diffents scenarii :
     *
     * 1. Scenario 1 : We consider only file - this file (w/ same rights) is
     * located in 3 locations : a- /tmp/test_is.pdf b-
     * /tmp/test_same_is_with_other_name.pdf c- /tmp2/test_is.pdf Waited for
     * results : md5 checksum should be the same.
     *
     * 2. Scenario 2 : We consider 2 files : an right example of
     * IntegrationStatement (/tmp/test_md5_for_scenario2.pdf) and a dummy file
     * (a eg. a txt file with 'Hello' string inside) a- Start the test and note
     * on a paper the MD5 checksum results for the right
     * /tmp/test_md5_for_scenario2.pdf b- Move by hand your txt file to
     * /tmp/test_md5_for_scenario2.pdf c- Restart the test and compare the MD5
     * checksum results between fake /tmp/test_md5_for_scenario2.pdf and last
     * right /tmp/test_md5_for_scenario2.pdf. Waited for results : md5 checksum
     * should be different.
     *
     *
     */
    /*
	public static void testMD5ChecksumWithFiles(Log log) {

		String md5Checksum1a = net.ihe.gazelle.common.authentication.Md5Encryption
				.calculateMD5ChecksumForFile(new File("/tmp/test_md5.pdf"));
		String md5Checksum1b = net.ihe.gazelle.common.authentication.Md5Encryption
				.calculateMD5ChecksumForFile(new File("/tmp/test_md5_with_other_name.pdf"));
		String md5Checksum1c = net.ihe.gazelle.common.authentication.Md5Encryption
				.calculateMD5ChecksumForFile(new File("/tmp2/test_md5.pdf"));

		String md5Checksum2ac = net.ihe.gazelle.common.authentication.Md5Encryption
				.calculateMD5ChecksumForFile(new File("/tmp/test_md5_for_scenario2.pdf"));


	}
	*/
}
