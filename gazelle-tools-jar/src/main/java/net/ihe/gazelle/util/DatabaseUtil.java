package net.ihe.gazelle.util;

public class DatabaseUtil
{

   public static StringBuffer addANDorWHERE( StringBuffer currentQueryString  )
   {
   if ( currentQueryString.indexOf("WHERE") > 0 )
   return currentQueryString.append(" AND " ) ;
   else 
   return currentQueryString.append(" WHERE " ) ;

   }
}
