package net.ihe.gazelle.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class URLParser {

    private static final Logger LOG = LoggerFactory.getLogger(URLParser.class);

    public static final String TEST_INSTANCE_ID = "testInstanceId";
    public static final String CALLING_TOOL_OID = "callingToolOid";

    private int testingInstanceId;
    private int testingSessionId;

    public int getTestingInstanceId() {
        return testingInstanceId;
    }

    public void setTestingInstanceId(int testingInstanceId) {
        this.testingInstanceId = testingInstanceId;
    }

    public int getTestingSessionId() {
        return testingSessionId;
    }

    public void setTestingSessionId(int testingSessionId) {
        this.testingSessionId = testingSessionId;
    }

    public String replaceParams(String url, String testInstanceId, String callingToolOid) {
        url = replaceParam(url, TEST_INSTANCE_ID, testInstanceId);
        url = replaceParam(url, CALLING_TOOL_OID, callingToolOid);
        return url;
    }

    private String replaceParam(String url, final String paramName, String replaceValue) {
        String testingInstanceIdReg = paramName + "=([^&]*)";
        Pattern regex = Pattern.compile(testingInstanceIdReg, Pattern.DOTALL);
        Matcher regexMatcher = regex.matcher(url);
        if (regexMatcher.find()) {
            url = regexMatcher.replaceAll(paramName + "=" + replaceValue);
        }
        return url;
    }


}