/*
 * Copyright 2008 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.ihe.gazelle.util;

 
/**
 *  <b>Class Description :  </b>Pair<br><br>
 * @package	net.ihe.gazelle.pr.systems.model
 * @author		Jean-Baptiste Meyer / INRIA Rennes IHE development Project
 * @see	> 	jmeyer@irisa.fr  -  http://www.ihe-europe.org
 * @version		1.0 - 2007, December 21
 * @param <T>
 * @param <U>
 *
 */

 
public class Pair<T, U> implements java.io.Serializable {

	
	/**
    * 
    */
   private static final long serialVersionUID = 1L;

   T object1;

   U object2;

   public Pair(T o1, U o2) {
      object1 = o1;
      object2 = o2;
   }

   public T getObject1() {
     return object1;
   }

   public U getObject2() {
     return object2;
   }

   public void setObject1(T object1)
   {
      this.object1 = object1;
   }

   public void setObject2(U object2)
   {
      this.object2 = object2;
   }

@Override
public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((object1 == null) ? 0 : object1.hashCode());
	result = prime * result + ((object2 == null) ? 0 : object2.hashCode());
	return result;
}

@Override
public boolean equals(Object obj) {
	if (this == obj) {
		return true;
	}
	if (obj == null) {
		return false;
	}
	if (getClass() != obj.getClass()) {
		return false;
	}
	Pair other = (Pair) obj;
	if (object1 == null) {
		if (other.object1 != null) {
			return false;
		}
	} else if (!object1.equals(other.object1)) {
		return false;
	}
	if (object2 == null) {
		if (other.object2 != null) {
			return false;
		}
	} else if (!object2.equals(other.object2)) {
		return false;
	}
	return true;
}
   
   
   
}